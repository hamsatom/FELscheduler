> **Podrobný návod k použití je sepsán v [Manual.pdf](Manual.pdf)**

[![pipeline status](https://gitlab.fel.cvut.cz/hamsatom/FELscheduler/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/hamsatom/FELscheduler/commits/master) [![coverage report](https://gitlab.fel.cvut.cz/hamsatom/FELscheduler/badges/master/coverage.svg)](https://gitlab.fel.cvut.cz/hamsatom/FELscheduler/commits/master)

# [LATEST BUILD](https://gitlab.fel.cvut.cz/hamsatom/FELscheduler/-/jobs/artifacts/master/raw/Rozvrhar.jar?job=build-job)
**Autor:** Tomáš Hamsa  
**Javadoc:** http://hamsatom.pages.fel.cvut.cz/FELscheduler/

Tento projekt si klade za cíl vytvořit aplikaci schopnou sestavit rozvrh podle několika kritérií. Aplikace má zjednodušit studentům stavbu rozvrhů a hledání předmětů v KOSu.  
Chceme, aby aplikace poskytovala vždy aktuální informace, ale, aby také byla schopná pracovat offline. Abychom získali vždy aktuální informace, použijeme webovou službu KOS API.  
Nedílnou součástí aplikace je také grafické rozhraní v JavaFX, které je navržené intuitivně. Chceme, ale také poskytnout možnost zadat data mimo grafického rozhraní a to konfigurací YAML souboru.