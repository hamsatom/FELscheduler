package logic;


import java.util.Optional;
import logic.data.StudentService;
import model.objects.Student;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 19.11.2017.
 */
public class StudentServiceImplTest {

  @Test
  public void testGetDbStudent() {
    Optional<Student> studentOptional = StudentService.getDbStudent("unknownStudent");
    Assert.assertFalse(studentOptional.isPresent());
  }

  @Test
  public void testDBorNewStudent() {
    Student student = new Student().setCode("unknownStudent");
    Student studentOptional = StudentService.getStudentOrNew("unknownStudent");
    Assert.assertEquals(studentOptional, student);
  }
}