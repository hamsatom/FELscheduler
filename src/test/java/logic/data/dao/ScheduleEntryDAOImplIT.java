package logic.data.dao;


import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nonnull;
import logic.data.dao.template.ScheduleEntryDAO;
import model.objects.ScheduleEntry;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 19.11.2017.
 */
public class ScheduleEntryDAOImplIT {

  private ScheduleEntryDAO scheduleEntryDAO;

  @BeforeMethod
  public void setUp() {
    scheduleEntryDAO = new ScheduleEntryDAOImpl();
  }

  @Test
  public void testPersist() {
    scheduleEntryDAO.delete(persistScheduleEntry().getId());
  }

  @Nonnull
  private ScheduleEntry persistScheduleEntry() {
    ScheduleEntry scheduleEntry = new ScheduleEntry();
    Optional<ScheduleEntry> savedScheduleEntry = scheduleEntryDAO.save(scheduleEntry);
    Assert.assertTrue(savedScheduleEntry.isPresent());
    Assert.assertEquals(savedScheduleEntry.get(), scheduleEntry);
    return savedScheduleEntry.get();
  }

  @Test
  public void testList() {
    Collection<ScheduleEntry> savedScheduleEntry = Arrays
        .asList(persistScheduleEntry(), persistScheduleEntry());
    Collection<ScheduleEntry> foundScheduleEntries = scheduleEntryDAO.list();
    Assert.assertTrue(foundScheduleEntries.containsAll(savedScheduleEntry));
    savedScheduleEntry.parallelStream().map(ScheduleEntry::getId).forEach(scheduleEntryDAO::delete);
  }

  @Test
  public void testUpdate() {
    ScheduleEntry savedScheduleEntry = persistScheduleEntry();
    savedScheduleEntry.setEndHour(Short.MAX_VALUE);
    Optional<ScheduleEntry> updatedScheduleEntry = scheduleEntryDAO
        .update(savedScheduleEntry);
    Assert.assertTrue(updatedScheduleEntry.isPresent());
    Assert.assertEquals(updatedScheduleEntry.get(), savedScheduleEntry);
    scheduleEntryDAO.delete(savedScheduleEntry.getId());
  }
}