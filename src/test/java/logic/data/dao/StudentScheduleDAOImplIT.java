package logic.data.dao;


import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nonnull;
import logic.data.dao.template.StudentScheduleDAO;
import model.objects.StudentSchedule;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 19.11.2017.
 */
public class StudentScheduleDAOImplIT {

  private StudentScheduleDAO studentScheduleDAO;

  @BeforeMethod
  public void setUp() {
    studentScheduleDAO = new StudentScheduleDAOImpl();
  }

  @Test
  public void testPersist() {
    studentScheduleDAO.delete(persistStudentSchedule().getId());
  }

  @Nonnull
  private StudentSchedule persistStudentSchedule() {
    StudentSchedule studentSchedule = new StudentSchedule().setStudentsInGroup("testStudent")
        .setSemester("B171");
    Optional<StudentSchedule> savedStudentSchedule = studentScheduleDAO.save(studentSchedule);
    Assert.assertTrue(savedStudentSchedule.isPresent());
    Assert.assertEquals(savedStudentSchedule.get(), studentSchedule);
    return savedStudentSchedule.get();
  }

  @Test
  public void testList() {
    Collection<StudentSchedule> savedStudentSchedule = Arrays
        .asList(persistStudentSchedule(), persistStudentSchedule());
    Collection<StudentSchedule> foundStudentSchedules = studentScheduleDAO.list();
    Assert.assertTrue(foundStudentSchedules.containsAll(savedStudentSchedule));
    savedStudentSchedule.parallelStream().map(StudentSchedule::getId).forEach(studentScheduleDAO::delete);
  }

  @Test
  public void testUpdate() {
    StudentSchedule savedStudentSchedule = persistStudentSchedule();
    savedStudentSchedule.setLastHour(Short.MAX_VALUE);
    Optional<StudentSchedule> updatedStudentSchedule = studentScheduleDAO
        .update(savedStudentSchedule);
    Assert.assertTrue(updatedStudentSchedule.isPresent());
    Assert.assertEquals(updatedStudentSchedule.get(), savedStudentSchedule);
    studentScheduleDAO.delete(savedStudentSchedule.getId());
  }
}