package logic.data.dao;


import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nonnull;
import logic.data.dao.template.StudentDAO;
import model.objects.Student;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 19.11.2017.
 */
public class StudentDAOImplIT {

  private StudentDAO studentDAO;

  @BeforeMethod
  public void setUp() {
    studentDAO = new StudentDAOImpl();
  }

  @Test
  public void testPersist() {
    studentDAO.delete(persistStudent().getCode());
  }

  @Test
  public void testFind() {
    Student student = persistStudent();

    Optional<Student> foundStudent = studentDAO.find(student.getCode());
    Assert.assertTrue(foundStudent.isPresent());
    Assert.assertEquals(foundStudent.get(), student);
    studentDAO.delete(student.getCode());
  }

  @Nonnull
  private Student persistStudent() {
    Student student = new Student().setCode(LocalDateTime.now().toString());
    Optional<Student> savedStudent = studentDAO.save(student);
    Assert.assertTrue(savedStudent.isPresent());
    Assert.assertEquals(savedStudent.get(), student);
    return student;
  }

  @Test
  public void testList() {
    Collection<Student> savedStudent = Arrays.asList(persistStudent(), persistStudent());
    Collection<Student> foundStudents = studentDAO.list();
    Assert.assertTrue(foundStudents.containsAll(savedStudent));
    savedStudent.parallelStream().map(Student::getCode).forEach(studentDAO::delete);
  }

  @Test
  public void testUpdate() {
    Student savedStudent = persistStudent();
    savedStudent.setEarliestOKHour(Short.MAX_VALUE);
    Optional<Student> updatedStudent = studentDAO.update(savedStudent);
    Assert.assertTrue(updatedStudent.isPresent());
    Assert.assertEquals(updatedStudent.get(), savedStudent);
    studentDAO.delete(savedStudent.getCode());
  }

  @Test
  public void testDelete() {
    Student savedStudent = persistStudent();
    studentDAO.delete(savedStudent.getCode());
    Optional<Student> foundStudent = studentDAO.find(savedStudent.getCode());
    Assert.assertFalse(foundStudent.isPresent());
    Assert.assertFalse(studentDAO.list().contains(savedStudent));
  }
}