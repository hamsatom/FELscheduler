package logic.schedule;

import java.util.Arrays;
import model.enums.ClassType;
import model.enums.Days;
import model.objects.Class;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class IndividualScheduleTest {

  private IndividualSchedule schedule;

  @BeforeMethod
  public void setUp() {
    schedule = new IndividualSchedule();
  }

  @Test
  public void testGetCopy() {
    Assert.assertEquals(schedule, schedule.getCopy());
  }

  @Test
  public void testGetFreeDaysCount() {
    Assert.assertEquals(schedule.getFreeDaysCount(), 5);
  }

  @Test
  public void testGetDaysWithOnlyTheoryLessonsCount() {
    Assert.assertEquals(schedule.getDaysWithOnlyTheoryLessonsCount(), 5);
  }

  @Test
  public void testCountHoursInSchool() {
    double count = schedule.countHoursInSchool((short) 3, (short) 12, 1.5);
    Assert.assertEquals(count, 0.0);
  }

  @Test
  public void testGet() {
    Class[][] entries = schedule.get();
    Assert.assertNotNull(entries);
    Arrays.stream(entries)
        .flatMap(Arrays::stream)
        .forEach(Assert::assertNull);
  }

  @Test
  public void testNotEmpty() {
    Days day = Days.PATEK;
    int dayNumber = day.getValue();
    short startHour = 15;
    short endHour = 16;
    double duration = endHour - startHour + 1.0;
    String teacher = "a";
    String room = "r";
    ClassType type = ClassType.CVICENI;

    Class expectedEntry = new Class()
        .setDay(day)
        .setStartHour(startHour)
        .setEndHour(endHour)
        .setRoom(room)
        .setType(type)
        .setTeacher(teacher);
    PrioritizedClass prioritizedClass = new PrioritizedClass(Short.MAX_VALUE, expectedEntry);
    schedule.addClass(prioritizedClass);

    double countedHours = schedule.countHoursInSchool(Short.MIN_VALUE, Short.MAX_VALUE, 1);
    Assert.assertEquals(countedHours, duration);
    Assert.assertEquals(schedule.getDaysWithOnlyTheoryLessonsCount(), 4);
    Assert.assertEquals(schedule.getFreeDaysCount(), 4);
    Assert.assertFalse(schedule.isSpaceEmpty(dayNumber, startHour, endHour, Short.MIN_VALUE));
    Assert.assertEquals(schedule.getCopy(), schedule);

    Class[][] classes = schedule.get();
    Assert.assertNotNull(classes);
    Assert.assertTrue(classes.length >= dayNumber);
    Assert.assertTrue(classes[0].length >= endHour);
    for (int i = 0; i < classes.length; i++) {
      for (int j = 0; j < classes[i].length; j++) {
        Class actualClass = classes[i][j];
        if (i == dayNumber && j >= startHour && j <= endHour) {
          Assert.assertEquals(actualClass, expectedEntry);
        } else {
          Assert.assertNull(actualClass);
        }
      }
    }
  }
}