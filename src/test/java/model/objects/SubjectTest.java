package model.objects;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * @author Tomas Hamsa on 15.01.2019 11:58
 */
public class SubjectTest {

	private static final String NAME_SHORTCUTS_PROVIDER = "providerNameShortcuts";

	private Subject subject;

	@DataProvider(name = NAME_SHORTCUTS_PROVIDER)
	private static Object[][] provideNameShortcuts() {
		return new Object[][]{
				{"Web 2.0", "MI-W20.16", "W20.16"},
				{"Datove sklady", "18DWH", "DWH"},
				{"Efektivni software", "B4M36ESW", "ESW"},
				{"Anglicka konverzace 2", "A0B04KA2", "KA2"},
				{"Kombinatoricka optimalizace", "B4M35KO", "KO"},
				{"Rusky jazyk 1", "A0B04R1", "R1"},
				{"Rustina 1", "04RZ1", "RZ1"},
				{"Uvod do programovani v Pythonu", "818UPYT", "UPYT"},
				{"Name", "C", "Name"},
				{"name", null, "name"},
				{"Programovani v C--", "B4M35C--", "C--"},
		};
	}

	@BeforeMethod
	public void setUp() {
		subject = new Subject();
	}

	@Test(priority = 1)
	public void testGetName() {
		Assert.assertNull(subject.getName(), "Name of empty subject not null");
	}

	@Test(priority = 1)
	public void testGetCode() {
		Assert.assertNull(subject.getCode(), "Code of empty subject not null");
	}

	@Test(priority = 2)
	public void testSetName() {
		String name = "testName";
		Assert.assertEquals(subject.setName(name), subject, "Incorrect builder reference");
		Assert.assertEquals(subject.getName(), name, "Different name of subject than set");
	}

	@Test(priority = 2)
	public void testSetCode() {
		String code = "testCode";
		Assert.assertEquals(subject.setCode(code), subject, "Incorrect builder reference");
		Assert.assertEquals(subject.getCode(), code, "Different code of subject than set");
	}

	@Test(priority = 3, dataProvider = NAME_SHORTCUTS_PROVIDER)
	public void testGetNameShortcut(String name, String code, String expectedNameShortcut) {
		subject.setName(name);
		subject.setCode(code);
		Assert.assertEquals(subject.getNameShortcut(), expectedNameShortcut, "Incorrect subject name shortcut");
	}
}