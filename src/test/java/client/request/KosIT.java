package client.request;

import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.io.IOException;

@Ignore("KOS API currently doesn't work")
public class KosIT {

  private static final String PROGRAMME = "BP4";
  private static final String SUBJECT_CODE = "A0B04KA";
  private static final String SUBJECT_NAME = "Anglická konverzace";
  private static final String SEMESTER = Kos.getCurrentSemesterCode();

  @Test
  public void testGetCourseParallels() throws IOException {
    boolean notEmpty = Kos.getCourseParallels(ApiBase.FEL, SEMESTER, SUBJECT_CODE, null).findAny()
        .isPresent();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetAllProgrammes() throws IOException {
    boolean notEmpty = Kos.getAllProgrammes(ApiBase.FIT, SEMESTER, null).findAny().isPresent();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetProgrammeCourses() throws IOException {
    boolean notEmpty = Kos.getProgrammeCourses(ApiBase.FEL, SEMESTER, null, PROGRAMME).findAny()
        .isPresent();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetAllCourses() throws IOException {
    boolean notEmpty = Kos.getAllCourses(ApiBase.FIT, SEMESTER, null).findAny().isPresent();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetCourseName() throws IOException {
    String courseName = Kos.getCourseName(ApiBase.FEL, SEMESTER, SUBJECT_CODE);
    Assert.assertEquals(courseName, SUBJECT_NAME);
  }

}