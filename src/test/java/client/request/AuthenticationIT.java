package client.request;

import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.io.IOException;

@Ignore("KOS API currently doesn't work")
public class AuthenticationIT {

  @Test
  public void testGetToken() throws IOException {
    String originalToken = Authentication.getToken();
    Assert.assertEquals(Authentication.getToken(), originalToken);
  }
}