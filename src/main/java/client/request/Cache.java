package client.request;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

/**
 * Cache with logic.data stored inside it. The logic.data are stored in byte array using {@link
 * SoftReference} so if the JVM needs more space than the reference are dropped and object deleted.
 * That should prevent OutOfMemoryException.
 *
 * @author Tomáš Hamsa on 02.04.2017.
 */
final class Cache {

  /**
   * Cache with logic.data from files
   */
  private static final Map<String, SoftReference<String>> REFERENCE_CACHE = new ConcurrentHashMap<>();

  /**
   * As this is utility class with static methods and field this class should not be ever
   * initialized
   *
   * @throws IllegalAccessException if someone tries to initialize this class
   */
  private Cache() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  /**
   * Provides logic.data from cache. This method is exception-safe and doesn't throw any
   * RuntimeException. In case logic.data is not present in cache or exception happened in the
   * process of obtaining logic.data than this class returns {@code Observable.empty ()} but doesn't
   * throw any exception. This method is thread-safe without synchronization so it can be accessed
   * from multiple instances at once without blocking.
   *
   * @param request absolute path of file with name of the file. It's used as a key in cache so all
   * logic.data that the file contains are assigned to this given key.
   * @return Observable of logic.data. Observable obtains logic.data if the logic.data are present
   * in cache and are not enqued to be deleted. Otherwise returns empty Observable
   */
  @Nonnull
  static Optional<String> getData(@Nonnull String request) {
    return Stream.of(REFERENCE_CACHE.get(request))
        .filter(Objects::nonNull)
        .filter(reference -> !reference.isEnqueued())
        .map(SoftReference::get)
        .findAny();
  }

  /**
   * Saves provided logic.data to cache
   *
   * @param request absolute path of the file used as a key in cache
   * @param data logic.data that the file contains, used as a value in cache
   */
  static void cacheInMemory(@Nonnull String request, @Nonnull String data) {
    REFERENCE_CACHE.put(request, new SoftReference<>(data));
  }
}
