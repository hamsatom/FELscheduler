package client.request;

import client.dto.objects.Course;
import client.dto.objects.InternalCourseEnrollment;
import client.dto.objects.InternalCourseEnrollment.InternalCourse;
import client.dto.objects.Parallel;
import client.dto.objects.Programme;
import client.dto.objects.TimetableSlot;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.intabulas.sandler.elements.Feed;
import org.intabulas.sandler.parser.AtomParser;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
public final class Kos {

  private static final AtomParser ATOM_PARSER = new AtomParser();
  private static final XStream X_STREAM = new XStream();
  private static final Pattern PATTERN_AND = Pattern.compile("&");

  static {
    X_STREAM.registerConverter(
        new ToAttributedValueConverter(InternalCourse.class,
            X_STREAM.getMapper(), X_STREAM.getReflectionProvider(), X_STREAM.getConverterLookup(),
            "name"));
    X_STREAM.aliasAttribute(InternalCourse.class, "courseCode", "xlink:href");

    X_STREAM.addImplicitCollection(Parallel.class, "teacher", String.class);
    X_STREAM.addImplicitCollection(Parallel.class, "timetableSlot", TimetableSlot.class);

    Class<?>[] classes = new Class[]{Course.class, InternalCourseEnrollment.class,
        Parallel.class, Programme.class, TimetableSlot.class};
    XStream.setupDefaultSecurity(X_STREAM);
    X_STREAM.allowTypes(classes);

    Arrays.stream(classes)
        .forEach(aClass -> X_STREAM.alias(aClass.getSimpleName(), aClass));
  }

  private Kos() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static Stream<InternalCourseEnrollment> getStudentCourses(@Nonnull ApiBase location,
      String semesterCode, @Nonnull String studentUsernameOrId)
      throws IOException {

    String result = Requests.getStudentCourses(location, studentUsernameOrId, semesterCode);

    return parse(result, InternalCourseEnrollment.class);
  }

  public static Stream<Parallel> getCourseParallels(@Nonnull ApiBase location,
      String semesterCode, String courseCode, @Nullable String query)
      throws IOException {

    String result = Requests.getCourseParallels(location, courseCode, semesterCode, query);

    return parse(result, Parallel.class);
  }

  public static Stream<Programme> getAllProgrammes(@Nonnull ApiBase location,
      String semesterCode, @Nullable String query)
      throws IOException {

    String result = Requests.getAllProgrammes(location, query, semesterCode);

    return parse(result, Programme.class);
  }

  public static Stream<Course> getProgrammeCourses(@Nonnull ApiBase location,
      String semesterCode, String query, String programmeCode)
      throws IOException {

    String result = Requests
        .getProgrammeCourses(location, query, programmeCode, semesterCode);

    return parse(result, Course.class);
  }

  public static Stream<Course> getAllCourses(@Nonnull ApiBase location,
      @Nonnull String semesterCode, @Nullable String query)
      throws IOException {

    String result = Requests.getAllCourses(location, query, semesterCode);

    return parse(result, Course.class);
  }

  public static String getCourseName(@Nonnull ApiBase location, String semesterCode,
      String courseCode) throws IOException {

    String result = Requests.getCourseName(location, courseCode, semesterCode);
    return parseSingleTag(result, "name");
  }

  private static String parseSingleTag(@Nonnull String kosResult, String tagName)
      throws IOException {
    int indexStart = kosResult.indexOf('<' + tagName);
    indexStart = kosResult.indexOf('>', indexStart) + 1;
    int indexEnd = kosResult.indexOf("</" + tagName + '>', indexStart);

    try {
      return kosResult.substring(indexStart, indexEnd);
    } catch (Exception e) {
      throw new IOException(
          "Chyba při získávání jedíného tagu \"" + tagName + "\" z KOS API odpovědi: " + kosResult);
    }
  }

  @SuppressWarnings("unchecked")
  private static <T> Stream<T> parse(@Nonnull String atomXml, @Nonnull Class<T> clazz)
      throws IOException {

    if (!atomXml.contains("<atom:entry>")) {
      return Stream.empty();
    }

    try {
      Feed feed;
      try (InputStream stream = new ByteArrayInputStream(atomXml.getBytes(Constants.CHARSET))) {
        feed = ATOM_PARSER.parseInput(stream);
      }

      Builder<T> streamBuilder = Stream.builder();

      for (int i = 0; i < feed.getEntryCount(); i++) {

        String xml = String
            .format("<%s>%s</%s>", clazz.getSimpleName(), feed.getEntry(i).getContent(0).getBody(),
                clazz.getSimpleName());
        xml = PATTERN_AND.matcher(xml).replaceAll(" and ");

        T entity = (T) X_STREAM.fromXML(xml);

        streamBuilder.accept(entity);
      }

      return streamBuilder.build();
    } catch (Exception e) {
      throw new IOException(
          "Chyba při získávání \"" + clazz.getSimpleName() + "\" z KOS API odpovědi: " + atomXml);
    }
  }

  public static String getCurrentSemesterCode() {
    int lastTwoDigits = LocalDate.now().getYear() % 100;

    int semester;
    int currentMonth = LocalDate.now().getMonthValue();
    if (currentMonth > 3 && currentMonth < 11) {
      semester = 1;
    } else {
      if (currentMonth < 4) {
        --lastTwoDigits;
      }
      semester = 2;
    }

    return String.format("B%d%d", lastTwoDigits, semester);
  }


}
