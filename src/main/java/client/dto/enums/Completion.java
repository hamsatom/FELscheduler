package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
public enum Completion {
  CLFD_CREDIT("klasifikovaný zápočet"), CREDIT("zápočet"), CREDIT_EXAM(
      "zápočet a zkouška"), DEFENCE("obhajoba"), EXAM("zkouška"), NOTHING(
      "nic"), UNDEFINED("nedefinovaný");

  private final String czechName;

  Completion(String czechName) {
    this.czechName = czechName;
  }

  @Override
  public String toString() {
    return czechName;
  }
}
