package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
public enum ProgrammeType {
  BACHELOR("bakalářské"), DOCTORAL("doktorské"), INTERNSHIP("stáž"), LIFELONG(
      "celoživotní"), MASTER("magisterské"), MASTER_LEGACY("magisterské (původní)"), UNDEFINED(
      "neurčeno");

  private final String czechName;

  ProgrammeType(String czechName) {
    this.czechName = czechName;
  }

  @Override
  public String toString() {
    return czechName;
  }
}
