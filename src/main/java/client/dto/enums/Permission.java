package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public enum Permission {
  ALLOWED("povoleno"), DENIED("zakázáno"), UNDEFINED("nedefinovaný");

  private final String czechName;

  Permission(String czechName) {
    this.czechName = czechName;
  }

  public String getValue() {
    return czechName;
  }
}
