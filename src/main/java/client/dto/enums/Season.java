package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
public enum Season {
  WINTER("zimní"), SUMMER("letní"), BOTH("oba"), UNDEFINED("neurčená");

  private final String czechName;

  Season(String czechName) {
    this.czechName = czechName;
  }

  @Override
  public String toString() {
    return czechName;
  }
}
