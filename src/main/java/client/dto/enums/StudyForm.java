package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
public enum StudyForm {
  FULLTIME("prezenční"), PARTTIME("kombinované"), DISTANCE("dálkové"), SELF_PAYER(
      "pro samoplátce (anglické)"), UNDEFINED("neurčeno"), Z("Z");

  private final String czechName;

  StudyForm(String czechName) {
    this.czechName = czechName;
  }

  @Override
  public String toString() {
    return czechName;
  }
}
