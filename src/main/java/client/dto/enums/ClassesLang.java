package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
public enum ClassesLang {
  CS("čeština"), DE("němčina"), EN("angličtina"), ES("španělština"), FR("francouzština"), PL(
      "polština"), RU("ruština"), SK("slovenština"), UNDEFINED("neurčen");

  private final String czechName;

  ClassesLang(String czechName) {
    this.czechName = czechName;
  }

  public String getValue() {
    return czechName;
  }

}
