package client.dto.enums;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public enum ParallelType {
  LABORATORY("laboratoř"), LECTURE("přednáška"), TUTORIAL("cvičení"), UNDEFINED("nedefinovaný");

  private final String czechName;

  ParallelType(String czechName) {
    this.czechName = czechName;
  }

  public String getValue() {
    return czechName;
  }
}
