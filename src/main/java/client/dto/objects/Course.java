package client.dto.objects;

import client.dto.enums.ClassesLang;
import client.dto.enums.Completion;
import client.dto.enums.ProgrammeType;
import client.dto.enums.Season;
import client.dto.enums.StudyForm;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public final class Course implements Serializable {

  private static final long serialVersionUID = 6262182418933072798L;

  private ClassesLang classesLang;
  private String code;
  private Completion completion;
  private short credits;
  private String department;
  private String name;
  private ProgrammeType programmeType;
  private Season season;
  private StudyForm studyForm;

  public ClassesLang getClassesLang() {
    return classesLang;
  }

  public String getCode() {
    return code;
  }

  public Completion getCompletion() {
    return completion;
  }

  public short getCredits() {
    return credits;
  }

  public String getDepartment() {
    return department;
  }

  public String getName() {
    return name;
  }

  public ProgrammeType getProgrammeType() {
    return programmeType;
  }

  public Season getSeason() {
    return season;
  }

  public StudyForm getStudyForm() {
    return studyForm;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Course)) {
      return false;
    }
    Course course = (Course) o;
    return credits == course.credits &&
        classesLang == course.classesLang &&
        Objects.equals(code, course.code) &&
        completion == course.completion &&
        Objects.equals(department, course.department) &&
        Objects.equals(name, course.name) &&
        programmeType == course.programmeType &&
        season == course.season &&
        studyForm == course.studyForm;
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(classesLang, code, completion, credits, department, name, programmeType, season,
            studyForm);
  }
}
