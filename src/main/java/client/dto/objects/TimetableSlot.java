package client.dto.objects;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public final class TimetableSlot implements Serializable {

  private static final long serialVersionUID = 1961048687126642441L;
  private int day;
  private int duration;
  private int firstHour;
  private String room;

  public int getDay() {
    return day;
  }

  public int getDuration() {
    return duration;
  }

  public int getFirstHour() {
    return firstHour;
  }

  public String getRoom() {
    return room;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TimetableSlot)) {
      return false;
    }
    TimetableSlot that = (TimetableSlot) o;
    return day == that.day &&
        duration == that.duration &&
        firstHour == that.firstHour &&
        Objects.equals(room, that.room);
  }

  @Override
  public int hashCode() {

    return Objects.hash(day, duration, firstHour, room);
  }
}
