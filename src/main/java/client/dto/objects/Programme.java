package client.dto.objects;

import client.dto.enums.ClassesLang;
import client.dto.enums.ProgrammeType;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public final class Programme implements Serializable {

  private static final long serialVersionUID = -4983042224764632770L;

  private ClassesLang classesLang;
  private String code;
  private String diplomaName;
  private String faculty;
  private boolean openForAdmission;
  private float studyDuration;
  private ProgrammeType type;

  public ClassesLang getClassesLang() {
    return classesLang;
  }

  public String getCode() {
    return code;
  }

  public String getDiplomaName() {
    return diplomaName;
  }

  public String getFaculty() {
    return faculty;
  }

  public boolean isOpenForAdmission() {
    return openForAdmission;
  }

  public float getStudyDuration() {
    return studyDuration;
  }

  public ProgrammeType getType() {
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Programme)) {
      return false;
    }
    Programme programme = (Programme) o;
    return openForAdmission == programme.openForAdmission &&
        Float.compare(programme.studyDuration, studyDuration) == 0 &&
        classesLang == programme.classesLang &&
        Objects.equals(code, programme.code) &&
        Objects.equals(diplomaName, programme.diplomaName) &&
        Objects.equals(faculty, programme.faculty) &&
        type == programme.type;
  }

  @Override
  public int hashCode() {

    return Objects
        .hash(classesLang, code, diplomaName, faculty, openForAdmission, studyDuration, type);
  }
}