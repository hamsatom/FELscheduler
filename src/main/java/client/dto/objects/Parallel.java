package client.dto.objects;

import client.dto.enums.ParallelType;
import client.dto.enums.Permission;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public final class Parallel implements Serializable {

  private static final long serialVersionUID = 8031428947491092366L;

  private ParallelType parallelType;
  private List<String> teacher;
  private List<TimetableSlot> timetableSlot;
  private short capacity;
  private short occupied;
  private Permission capacityOverfill;

  public List<TimetableSlot> getTimetableSlot() {
    return timetableSlot;
  }

  public ParallelType getParallelType() {
    return parallelType;
  }

  public List<String> getTeacher() {
    return teacher;
  }

  public short getCapacity() {
    return capacity;
  }

  public short getOccupied() {
    return occupied;
  }

  public Permission getCapacityOverfill() {
    return capacityOverfill;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Parallel)) {
      return false;
    }
    Parallel parallel = (Parallel) o;
    return capacity == parallel.capacity &&
        occupied == parallel.occupied &&
        parallelType == parallel.parallelType &&
        Objects.equals(teacher, parallel.teacher) &&
        Objects.equals(timetableSlot, parallel.timetableSlot) &&
        capacityOverfill == parallel.capacityOverfill;
  }

  @Override
  public int hashCode() {

    return Objects.hash(parallelType, teacher, timetableSlot, capacity, occupied, capacityOverfill);
  }
}
