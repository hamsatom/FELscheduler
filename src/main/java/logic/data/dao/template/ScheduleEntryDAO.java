package logic.data.dao.template;


import model.objects.ScheduleEntry;

/**
 * @author Tomáš Hamsa on 19.11.2017.
 */
public interface ScheduleEntryDAO extends DAO<ScheduleEntry> {

}
