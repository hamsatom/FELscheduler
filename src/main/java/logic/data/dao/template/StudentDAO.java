package logic.data.dao.template;


import model.objects.Student;

public interface StudentDAO extends DAO<Student> {

}
