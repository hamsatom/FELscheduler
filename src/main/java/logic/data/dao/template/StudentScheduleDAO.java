package logic.data.dao.template;


import model.objects.StudentSchedule;

/**
 * @author Tomáš Hamsa on 13.10.2017.
 */
public interface StudentScheduleDAO extends DAO<StudentSchedule> {

}
