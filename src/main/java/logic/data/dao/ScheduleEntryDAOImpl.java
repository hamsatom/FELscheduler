package logic.data.dao;


import logic.data.dao.template.ScheduleEntryDAO;
import model.objects.ScheduleEntry;

/**
 * @author Tomáš Hamsa on 19.11.2017.
 */
public final class ScheduleEntryDAOImpl extends AbstractDAO<ScheduleEntry> implements
    ScheduleEntryDAO {

}
