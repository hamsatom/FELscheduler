package logic.data.dao;


import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import logic.data.dao.template.DAO;

/**
 * @author Tomáš Hamsa
 */
abstract class AbstractDAO<T extends Serializable> implements DAO<T> {

  private static final EntityManagerFactory MANAGER_FACTORY = Persistence
      .createEntityManagerFactory("persistenceUnit");

  private final Class<T> type;


  @SuppressWarnings("unchecked")
  AbstractDAO() {
    Type t = getClass().getGenericSuperclass();
    ParameterizedType pt = (ParameterizedType) t;
    type = (Class<T>) pt.getActualTypeArguments()[0];
  }

  @Override
  @Nonnull
  public List<T> list() {
    EntityManager em = MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = em.getTransaction();
    try {
      transaction.begin();

      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<T> cq = cb.createQuery(type);
      Root<T> rootEntry = cq.from(type);
      CriteriaQuery<T> all = cq.select(rootEntry);
      TypedQuery<T> allQuery = em.createQuery(all);
      List<T> result = allQuery.getResultList();
      transaction.commit();
      return result;

    } finally {
      try {
        if (transaction.isActive()) {
          transaction.rollback();
        }
      } finally {
        em.close();
      }
    }
  }


  @Nonnull
  @Override
  public Optional<T> save(@Nonnull T entity) {
    EntityManager em = MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = em.getTransaction();
    try {
      transaction.begin();

      em.persist(entity);

      transaction.commit();
    } finally {
      try {
        if (transaction.isActive()) {
          transaction.rollback();
        }
      } finally {
        em.close();
      }
    }

    return Optional.ofNullable(entity);
  }

  @Nonnull
  @Override
  public Optional<T> find(@Nonnull Serializable id) {
    EntityManager em = MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = em.getTransaction();
    try {
      transaction.begin();

      T result = em.find(type, id);

      transaction.commit();
      return Optional.ofNullable(result);
    } finally {
      try {
        if (transaction.isActive()) {
          transaction.rollback();
        }
      } finally {
        em.close();
      }
    }
  }

  @Nonnull
  @Override
  public Optional<T> update(@Nonnull T entity) {
    EntityManager em = MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = em.getTransaction();
    try {
      transaction.begin();

      em.merge(entity);

      transaction.commit();
    } finally {
      try {
        if (transaction.isActive()) {
          transaction.rollback();
        }
      } finally {
        em.close();
      }
    }

    return Optional.ofNullable(entity);
  }

  @Override
  public void delete(@Nonnull Serializable id) {
    EntityManager em = MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = em.getTransaction();
    try {
      transaction.begin();

      em.remove(em.getReference(type, id));

      transaction.commit();
    } finally {
      try {
        if (transaction.isActive()) {
          transaction.rollback();
        }
      } finally {
        em.close();
      }
    }
  }
}