package logic.data.dao;


import logic.data.dao.template.StudentScheduleDAO;
import model.objects.StudentSchedule;

/**
 * @author Tomáš Hamsa on 13.10.2017.
 */
public final class StudentScheduleDAOImpl extends AbstractDAO<StudentSchedule> implements
    StudentScheduleDAO {

}
