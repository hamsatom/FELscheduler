package logic.data;

import client.dto.objects.Parallel;
import client.request.ApiBase;
import client.request.Kos;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import logic.data.dao.StudentDAOImpl;
import logic.data.dao.template.DAO;
import logic.mapper.SubjectDownloader;
import model.objects.Student;
import model.objects.Subject;

/**
 * @author Tomáš Hamsa on 08.10.2017.
 */
public final class StudentService {

  private static final Logger LOG = Logger.getLogger(StudentService.class.getName());

  private static final DAO<Student> STUDENT_DAO = new StudentDAOImpl();

  private StudentService() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static List<Subject> downloadEnrolledCourses(@Nonnull Student student,
      @Nonnull String semester) {
    try {
      return Kos.getStudentCourses(ApiBase.FEL, semester, student.getCode()).parallel()
          .map(internalCourseEnrollment -> {

            String query =
                student.isUseClosedClasses() ? null : SubjectDownloader.QUERY_NOT_CLOSED;
            Predicate<Parallel> filter =
                student.isUseFullClasses() ? SubjectDownloader.FILTER_NOTHING
                    : SubjectDownloader.FILTER_FULL;

            try {
              return SubjectDownloader
                  .download(ApiBase.FEL, semester, internalCourseEnrollment.getCourseCode(),
                      internalCourseEnrollment.getName(), query, filter);
            } catch (IOException e) {
              LOG.log(Level.INFO, "Error while downloading data from KOS", e);
            }
            return null;
          }).filter(Objects::nonNull)
          .collect(Collectors.toList());
    } catch (IOException e) {
      LOG.log(Level.INFO, "Error while downloading data from KOS", e);
    }
    return Collections.emptyList();
  }

  public static Student getStudentOrNew(@Nonnull String code) {
    Optional<Student> dbStudent = STUDENT_DAO.find(code);
    return dbStudent.orElseGet(() -> new Student().setCode(code));
  }

  public static Optional<Student> getDbStudent(@Nonnull String code) {
    return STUDENT_DAO.find(code);
  }

  public static void addSchedulesToDBStudents(
      @Nonnull Collection<? extends Student> students) {
    students.parallelStream()
        .forEach(o -> {
          Optional<Student> dbStudent = STUDENT_DAO.find(o.getCode());
          Student toPersist;
          if (dbStudent.isPresent()) {
            toPersist = dbStudent.get();
            toPersist.getSchedules().addAll(o.getSchedules());
          } else {
            toPersist = o;
          }
          STUDENT_DAO.update(toPersist);
        });
  }

  public static void updatePreferences(@Nonnull Student student) {
    Optional<Student> dbStudent = STUDENT_DAO.find(student.getCode());
    dbStudent.ifPresent(student1 -> student.getSchedules()
        .addAll(student1.getSchedules()));
    STUDENT_DAO.update(student);
  }
}
