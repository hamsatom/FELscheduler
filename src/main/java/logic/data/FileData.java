package logic.data;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;
import javax.annotation.Nonnull;
import model.objects.Subject;
import org.apache.commons.lang3.StringUtils;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;

/**
 * @author Tomáš Hamsa on 16.07.2017.
 */
public final class FileData {

  private static final Logger LOG = Logger.getLogger(FileData.class.getName());
  private static final Charset CODING = StandardCharsets.UTF_8;
  private static final String DEFAULT_YAML_FILE = "subjects.yaml";
  private static final ExecutorService THREAD_POOL = Executors.newSingleThreadExecutor();

  private static final Path rootYaml = Paths.get(".")
      .toAbsolutePath()
      .normalize()
      .resolve(DEFAULT_YAML_FILE);

  private static Path subjectsFile = Paths.get(DEFAULT_YAML_FILE).toAbsolutePath().normalize();

  private FileData() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static Path getSubjectsFile() {
    return subjectsFile;
  }

  public static void setSubjectsFile(@Nonnull String subjectsFile) {
    if (StringUtils.isBlank(subjectsFile)) {
      throw new IllegalArgumentException("File name is empty");
    }

    FileData.subjectsFile = Paths.get(subjectsFile).toAbsolutePath().normalize();
  }

  public static void saveAllSubjects(@Nonnull Iterator<Subject> subjects) {

    THREAD_POOL.submit(() -> {

      try (BufferedWriter writer = Files
          .newBufferedWriter(rootYaml, CODING, StandardOpenOption.CREATE,
              StandardOpenOption.TRUNCATE_EXISTING)) {

        Yaml yaml = new Yaml();
        yaml.setBeanAccess(BeanAccess.FIELD);
        yaml.dumpAll(subjects, writer);

      } catch (IOException e) {
        LOG.log(Level.WARNING, "Error while saving YAML", e);
      }
    });
  }

  public static Stream<Subject> loadAllSubjects() throws IOException {

    try (InputStream inStream = Files.newInputStream(subjectsFile, StandardOpenOption.READ)) {

      Yaml yaml = new Yaml();
      yaml.setBeanAccess(BeanAccess.FIELD);

      Builder<Subject> streamBuilder = Stream.builder();

      yaml.loadAll(inStream)
          .forEach(subject -> streamBuilder.accept((Subject) subject));

      return streamBuilder.build();
    }
  }
}
