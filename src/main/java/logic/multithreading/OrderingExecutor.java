package logic.multithreading;

import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Special executor which can order the tasks if a common key is given. Runnables submitted with
 * non-null key will guaranteed to run in order for the same key.
 */
public final class OrderingExecutor {

  private static final short TASKS_COUNT = (short) RunnableTags.values().length;
  private static final Collection<Runnable> EMPTY_QUEUE = new QueueWithHashCodeAndEquals<>();
  private static final ConcurrentMap<Object, Deque<Runnable>> TASK_MAP = new ConcurrentHashMap<>(
      TASKS_COUNT);
  private static final Executor THREAD_POOL = Executors.newFixedThreadPool(TASKS_COUNT);

  private OrderingExecutor() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing singleton");
  }

  public static void execute(@Nonnull Runnable runnable, @Nullable RunnableTags key) {
    if (key == null) {
      THREAD_POOL.execute(runnable);
      return;
    }

    Collection<Runnable> queueForKey = TASK_MAP.computeIfPresent(key, (k, v) -> {
      v.add(runnable);
      return v;
    });
    if (queueForKey == null) {
      // There was no running task with this key
      Deque<Runnable> newQ = new QueueWithHashCodeAndEquals<>();
      newQ.addLast(runnable);
      // Use putIfAbsent because this execute() method can be called concurrently as well
      queueForKey = TASK_MAP.putIfAbsent(key, newQ);
      if (queueForKey != null) {
        queueForKey.add(runnable);
      }
      THREAD_POOL.execute(new InternalRunnable(key));
    }
  }

  /**
   * Own Runnable used by OrderedExecutor. The runnable is associated with a specific key - the
   * Runnable for this key is polled. If the queue is empty, it tries to remove the queue from
   * TASK_MAP.
   */
  private static class InternalRunnable implements Runnable {

    private final RunnableTags key;

    private InternalRunnable(RunnableTags key) {
      this.key = key;
    }

    @Override
    public void run() {
      while (true) {
        // There must be at least one task now
        Runnable r = TASK_MAP.get(key).pollLast();
        TASK_MAP.get(key).clear();
        while (r != null) {
          r.run();
          r = TASK_MAP.get(key).pollLast();
          TASK_MAP.get(key).clear();
        }
        // The queue emptied
        // Remove from the map if and only if the queue is really empty
        boolean removed = TASK_MAP.remove(key, EMPTY_QUEUE);
        if (removed) {
          // The queue has been removed from the map,
          // if a new task arrives with the same key, a new InternalRunnable
          // will be created
          break;
        } // If the queue has not been removed from the map it means that someone put a task into it
        // so we can safely continue the loop
      }
    }
  }

  /**
   * Special Queue implementation, with equals() and hashCode() methods. By default, Java SE queues
   * use identity equals() and default hashCode() methods. This implementation uses
   * Arrays.equals(Queue::toArray()) and Arrays.hashCode(Queue::toArray()).
   *
   * @param <E> The type of elements in the queue.
   */
  private static class QueueWithHashCodeAndEquals<E> extends ConcurrentLinkedDeque<E> {

    private static final long serialVersionUID = 109026289737852859L;

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof QueueWithHashCodeAndEquals)) {
        return false;
      }
      Collection<?> other = (QueueWithHashCodeAndEquals<?>) obj;
      return Arrays.equals(toArray(), other.toArray());
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(toArray());
    }
  }
}