package logic.multithreading;

/**
 * @author Tomáš Hamsa on 17.09.2017.
 */
public enum RunnableTags {
  BUILD_SCHEDULES, PROGRAMMES, COURSES, ENROLLMENTS, UPDATE_PARALLELS, MY_GROUP_SCHEDULE, BUILD_GROUP_SCHEDULE

}
