package logic.multithreading;

import client.dto.objects.Course;
import client.dto.objects.Programme;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 17.09.2017.
 */
public final class ResultHolder {

  private static final Logger LOG = Logger.getLogger(ResultHolder.class.getName());
  private static final CountDownLatch coursesFinished = new CountDownLatch(1);
  private static final CountDownLatch programmesFinished = new CountDownLatch(1);
  private static volatile Collection<Course> courses;
  private static volatile Collection<Programme> programmes;

  private ResultHolder() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static Stream<Course> getCourses() {
    try {
      coursesFinished.await();
    } catch (InterruptedException e) {
      LOG.log(Level.WARNING, "Exception while waiting for courses", e);
      Thread.currentThread().interrupt();
    }
    return courses.stream();
  }

  public static void setCourses(@Nonnull Stream<Course> courses) {
    ResultHolder.courses = courses.collect(Collectors.toList());
    coursesFinished.countDown();
  }

  public static Stream<Programme> getProgrammes() {
    try {
      programmesFinished.await();
    } catch (InterruptedException e) {
      LOG.log(Level.WARNING, "Exception while waiting for programmes", e);
      Thread.currentThread().interrupt();
    }
    return programmes.stream();
  }

  public static void setProgrammes(@Nonnull Stream<Programme> programmes) {
    ResultHolder.programmes = programmes.collect(Collectors.toList());
    programmesFinished.countDown();
  }
}
