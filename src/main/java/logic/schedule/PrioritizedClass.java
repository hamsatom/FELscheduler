package logic.schedule;

import java.util.Objects;
import javax.annotation.Nonnull;
import model.objects.Class;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public final class PrioritizedClass {

  private final short priority;
  private final Class clazz;

  public PrioritizedClass(short priority, @Nonnull Class clazz) {
    this.priority = priority;
    this.clazz = clazz;
  }

  public short getPriority() {
    return priority;
  }

  @Nonnull
  public Class getClazz() {
    return clazz;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof PrioritizedClass)) {
      return false;
    }
    PrioritizedClass that = (PrioritizedClass) o;
    return priority == that.priority && Objects.equals(clazz, that.clazz);
  }

  @Override
  public int hashCode() {
    return Objects.hash(priority, clazz);
  }
}
