package logic.schedule;

import constant.ScheduleConstants;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiFunction;
import javax.annotation.Nonnull;
import model.enums.ClassType;
import model.enums.Locations;
import model.objects.Class;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public final class IndividualSchedule implements Schedule {

  private short freeDaysCount;
  private short daysWithOnlyTheoryLessonsCount;
  private short crossings;
  private boolean[] usedDay;
  private boolean[] dayWithLectures;
  private PrioritizedClass[][] scheduleArray;
  private short[] earliestHour;
  private short[] lastHour;


  public IndividualSchedule() {
    daysWithOnlyTheoryLessonsCount = freeDaysCount = ScheduleConstants.SCHOOL_DAYS_COUNT;
    crossings = 0;
    earliestHour = new short[ScheduleConstants.SCHOOL_DAYS_COUNT];
    Arrays.fill(earliestHour, Short.MAX_VALUE);
    lastHour = new short[ScheduleConstants.SCHOOL_DAYS_COUNT];
    usedDay = new boolean[ScheduleConstants.SCHOOL_DAYS_COUNT];
    dayWithLectures = new boolean[ScheduleConstants.SCHOOL_DAYS_COUNT];
    scheduleArray = new PrioritizedClass[ScheduleConstants.SCHOOL_DAYS_COUNT][ScheduleConstants.SCHOOL_HOURS_COUNT];
  }

  private static short findValue(@Nonnull short[] array,
      @Nonnull BiFunction<Short, Short, Short> compare) {

    short result = array[0];

    for (int i = 1; i < array.length; i++) {
      result = compare.apply(result, array[i]);
    }

    return result;
  }

  @Nonnull
  public IndividualSchedule getCopy() {
    IndividualSchedule copy = new IndividualSchedule();
    copy.freeDaysCount = freeDaysCount;
    copy.daysWithOnlyTheoryLessonsCount = daysWithOnlyTheoryLessonsCount;
    copy.crossings = crossings;
    copy.usedDay = Arrays.copyOf(usedDay, usedDay.length);
    copy.dayWithLectures = Arrays.copyOf(dayWithLectures, dayWithLectures.length);

    copy.scheduleArray = Arrays.stream(scheduleArray)
        .map(day -> Arrays.copyOf(day, day.length))
        .toArray(PrioritizedClass[][]::new);

    copy.earliestHour = Arrays.copyOf(earliestHour, earliestHour.length);
    copy.lastHour = Arrays.copyOf(lastHour, lastHour.length);

    return copy;
  }

  @Override
  public short getFreeDaysCount() {
    return freeDaysCount;
  }

  @Override
  public short getDaysWithOnlyTheoryLessonsCount() {
    return daysWithOnlyTheoryLessonsCount;
  }

  @Override
  public short getCrossings() {
    return crossings;
  }

  @Override
  public double countHoursInSchool(short firstOKHour, short lastOKHour, double penalty) {

    double result = 0.0;

    for (short day = 0; day < ScheduleConstants.SCHOOL_DAYS_COUNT; day++) {
      if (lastHour[day] == 0) {
        continue;
      }

      short firstHour = earliestHour[day];
      short latestHour = lastHour[day];

      result += latestHour - firstHour + 1;
      if (latestHour - lastOKHour > 0) {
        result += (latestHour - lastOKHour) * penalty;
      }
      if (firstOKHour - firstHour > 0) {
        result += (firstOKHour - firstHour) * penalty;
      }
    }

    return result;
  }

  @Nonnull
  public Class[][] get() {

    Class[][] classSchedule = new Class[ScheduleConstants.SCHOOL_DAYS_COUNT][ScheduleConstants.SCHOOL_HOURS_COUNT];

    for (int day = 0; day < ScheduleConstants.SCHOOL_DAYS_COUNT; day++) {
      for (int hour = 0; hour < ScheduleConstants.SCHOOL_HOURS_COUNT; hour++) {
        PrioritizedClass clas = scheduleArray[day][hour];

        if (clas != null) {
          classSchedule[day][hour] = clas.getClazz();
        }
      }
    }

    return classSchedule;
  }

  public void addClass(@Nonnull PrioritizedClass clazz) {

    Class cls = clazz.getClazz();
    int day = cls.getDay().getValue();
    short startHour = cls.getStartHour();
    short endHour = cls.getEndHour();

    if (!isSpaceEmpty(day, startHour, endHour, clazz.getPriority())) {
      return;
    }

    // if day was empty, decrease number of free days
    if (!usedDay[day]) {
      --freeDaysCount;
      usedDay[day] = true;
    }

    // if class is a theory lesson, update number of days with only theory lessons
    if (!dayWithLectures[day] && cls.getType() != ClassType.PREDNASKA) {
      --daysWithOnlyTheoryLessonsCount;
      dayWithLectures[day] = true;
    }

    // check for crossing
    if (scheduleArray[day][startHour - 1] != null &&
        scheduleArray[day][startHour - 1].getClazz().getLocation() != cls.getLocation() &&
        (cls.getLocation() != Locations.UNDEFINED ||
            scheduleArray[day][startHour - 1].getClazz().getLocation() != Locations.UNDEFINED
        )) {
      ++crossings;
    }
    if (endHour + 1 < ScheduleConstants.SCHOOL_HOURS_COUNT &&
        scheduleArray[day][endHour + 1] != null &&
        scheduleArray[day][endHour + 1].getClazz().getLocation() != cls.getLocation() &&
        (cls.getLocation() != Locations.UNDEFINED
            || scheduleArray[day][endHour + 1].getClazz().getLocation() != Locations.UNDEFINED)
    ) {
      ++crossings;
    }

    for (short hour = startHour; hour <= endHour; hour++) {
      scheduleArray[day][hour] = clazz;
    }

    earliestHour[day] = (short) Math.min(earliestHour[day], startHour);
    lastHour[day] = (short) Math.max(lastHour[day], endHour);
  }

  public boolean isSpaceEmpty(int day, short startHour, short endHour, short priority) {

    return Arrays.stream(scheduleArray[day], startHour, endHour + 1)
        .allMatch(cls -> cls == null || cls.getPriority() < priority);
  }

  @Override
  public short getFirstUsedHour() {
    return findValue(earliestHour, (aShort, aShort2) -> (short) Math.min(aShort, aShort2));
  }

  @Override
  public short getLastUsedHour() {
    return findValue(lastHour, (aShort, aShort2) -> (short) Math.max(aShort, aShort2));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof IndividualSchedule)) {
      return false;
    }
    IndividualSchedule that = (IndividualSchedule) o;
    return freeDaysCount == that.freeDaysCount &&
        daysWithOnlyTheoryLessonsCount == that.daysWithOnlyTheoryLessonsCount &&
        crossings == that.crossings &&
        Arrays.equals(usedDay, that.usedDay) &&
        Arrays.equals(dayWithLectures, that.dayWithLectures) &&
        Arrays.deepEquals(scheduleArray, that.scheduleArray) &&
        Arrays.equals(earliestHour, that.earliestHour) &&
        Arrays.equals(lastHour, that.lastHour);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(freeDaysCount, daysWithOnlyTheoryLessonsCount, crossings);
    result = 31 * result + Arrays.hashCode(usedDay);
    result = 31 * result + Arrays.hashCode(dayWithLectures);
    result = 31 * result + Arrays.hashCode(scheduleArray);
    result = 31 * result + Arrays.hashCode(earliestHour);
    result = 31 * result + Arrays.hashCode(lastHour);
    return result;
  }
}
