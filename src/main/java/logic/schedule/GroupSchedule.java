package logic.schedule;

import constant.ScheduleConstants;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import model.objects.Class;
import model.objects.Student;

/**
 * @author Tomáš Hamsa on 01.10.2017.
 */
public final class GroupSchedule implements Schedule {

  private static final short PEOPLE_TOGETHER_EXPONENT = 2;

  private Map<Student, IndividualSchedule> schedules;

  public GroupSchedule() {
    schedules = new ConcurrentHashMap<>();
  }

  @Nonnull
  public GroupSchedule getCopy() {
    GroupSchedule copy = new GroupSchedule();
    copy.schedules = schedules.entrySet()
        .stream()
        .collect(Collectors.toConcurrentMap(Entry::getKey,
            pair -> pair.getValue().getCopy()));
    return copy;
  }

  public double countClassesTogether() {
    List<Class[][]> groupClasses = schedules.values()
        .stream()
        .map(IndividualSchedule::get)
        .collect(Collectors.toList());

    Map<Class, Integer> repetitions;
    Class curClass;
    double sum = 0;
    for (int day = 0; day < ScheduleConstants.SCHOOL_DAYS_COUNT; day++) {
      for (int hour = 0; hour < ScheduleConstants.SCHOOL_HOURS_COUNT; hour++) {
        repetitions = new HashMap<>(groupClasses.size());
        for (Class[][] groupClass : groupClasses) {
          curClass = groupClass[day][hour];
          if (curClass == null) {
            continue;
          }
          repetitions.merge(curClass, 1, (a, b) -> a + b);
        }
        sum += repetitions.values()
            .stream()
            .filter(integer -> integer > 1)
            .mapToDouble(value -> Math.pow(value, PEOPLE_TOGETHER_EXPONENT))
            .sum();
      }
    }

    return sum;
  }

  public boolean isSpaceEmpty(@Nonnull Student person, int day, short startHour,
      short endHour, short priority) {

    IndividualSchedule schedule = get(person);
    return schedule == null || schedule.isSpaceEmpty(day, startHour, endHour, priority);
  }

  public void addClass(@Nonnull Student person, @Nonnull PrioritizedClass clazz) {
    IndividualSchedule schedule = getOrCreate(person);
    schedule.addClass(clazz);
  }

  public Stream<Student> getStudents() {
    return schedules.keySet()
        .stream();
  }

  public IndividualSchedule get(@Nonnull Student person) {
    return schedules.get(person);
  }

  public double countHoursInSchool(@Nonnull Student person, short firstOKHour,
      short lastOKHour, double penalty) {
    IndividualSchedule schedule = get(person);
    if (schedule == null) {
      throw new IllegalArgumentException(
          String.format("Student %s is not in schedule", person.getCode()));
    }
    return schedule.countHoursInSchool(firstOKHour, lastOKHour, penalty);
  }

  private IndividualSchedule getOrCreate(@Nonnull Student person) {
    return schedules.computeIfAbsent(person, e -> new IndividualSchedule());
  }

  public short getDaysWithOnlyTheoryLessonsCount(@Nonnull Student person) {
    IndividualSchedule schedule = schedules.get(person);
    if (schedule == null) {
      throw new IllegalArgumentException(
          String.format("Student %s is not in schedule", person.getCode()));
    }
    return schedule.getDaysWithOnlyTheoryLessonsCount();
  }

  @Override
  public short getFreeDaysCount() {
    return (short) schedules.values()
        .stream()
        .mapToInt(IndividualSchedule::getFreeDaysCount)
        .sum();
  }

  @Override
  public short getDaysWithOnlyTheoryLessonsCount() {
    return (short) schedules.values()
        .stream()
        .mapToInt(IndividualSchedule::getDaysWithOnlyTheoryLessonsCount)
        .sum();
  }

  @Override
  public short getCrossings() {
    return (short) schedules.values()
        .stream()
        .mapToInt(IndividualSchedule::getCrossings)
        .sum();
  }

  @Override
  public double countHoursInSchool(short firstOKHour, short lastOKHour,
      double penalty) {
    return schedules.values()
        .stream()
        .mapToDouble(schedule -> schedule.countHoursInSchool(firstOKHour, lastOKHour, penalty))
        .sum();
  }

  @Override
  public short getFirstUsedHour() {
    return schedules.values()
        .stream()
        .map(IndividualSchedule::getFirstUsedHour)
        .min(Comparator.comparingInt(a -> a))
        .orElseGet(() -> (short) 1);
  }

  @Override
  public short getLastUsedHour() {
    return schedules.values().stream()
        .map(IndividualSchedule::getFirstUsedHour)
        .max(Comparator.comparingInt(a -> a))
        .orElseGet(() -> (short) (ScheduleConstants.SCHOOL_HOURS_COUNT - 1));
  }

  public short getCrossings(@Nonnull Student student) {
    IndividualSchedule schedule = schedules.get(student);
    if (schedule == null) {
      throw new IllegalArgumentException(
          String.format("Student %s is not in schedule", student.getCode()));
    }
    return schedule.getCrossings();
  }

  public short getFreeDaysCount(@Nonnull Student student) {
    IndividualSchedule schedule = schedules.get(student);
    if (schedule == null) {
      throw new IllegalArgumentException(
          String.format("Student %s is not in schedule", student.getCode()));
    }
    return schedule.getFreeDaysCount();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof GroupSchedule)) {
      return false;
    }
    GroupSchedule that = (GroupSchedule) o;
    return Objects.equals(schedules, that.schedules);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schedules);
  }
}