package logic.schedule;

/**
 * @author Tomáš Hamsa on 01.10.2017.
 */
interface Schedule {

  short getFreeDaysCount();

  short getDaysWithOnlyTheoryLessonsCount();

  short getCrossings();

  double countHoursInSchool(short firstOKHour, short lastOKHour, double penalty);

  short getFirstUsedHour();

  short getLastUsedHour();
}
