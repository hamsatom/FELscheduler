package logic.mapper;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import logic.schedule.GroupSchedule;
import logic.schedule.IndividualSchedule;
import model.objects.Student;
import model.objects.StudentSchedule;

/**
 * @author Tomáš Hamsa on 12.10.2017.
 */
public final class GroupScheduleConverter {

  private static final BiFunction<? super IndividualSchedule, ? super String, StudentSchedule> convert = (schedule, names) -> new StudentSchedule()
      .setClasses(schedule.get())
      .setStudentsInGroup(names)
      .setFirstHour(schedule.getFirstUsedHour())
      .setLastHour(schedule.getLastUsedHour());

  private GroupScheduleConverter() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static List<Student> convert(@Nonnull Collection<? extends GroupSchedule> schedules,
      @Nonnull String semester) {

    List<Student> students = schedules.stream()
        .flatMap(GroupSchedule::getStudents)
        .distinct()
        .collect(Collectors.toList());

    students.forEach(student -> student.setSchedules(null));

    String studentsInGroup = students.stream()
        .map(Student::getCode)
        .collect(Collectors.joining(", "));

    students.forEach(student -> {
      List<StudentSchedule> studentSchedules = schedules.stream()
          .map(o -> o.get(student))
          .distinct()
          .map(schedule -> convert.apply(schedule, studentsInGroup)
              .setSemester(semester))
          .collect(Collectors.toList());

      student.getSchedules().addAll(studentSchedules);
    });

    return students;
  }
}
