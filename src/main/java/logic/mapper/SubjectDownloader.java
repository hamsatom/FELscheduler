package logic.mapper;

import client.dto.enums.ParallelType;
import client.dto.enums.Permission;
import client.dto.objects.Parallel;
import client.dto.objects.TimetableSlot;
import client.request.ApiBase;
import client.request.Kos;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import model.enums.ClassType;
import model.enums.Days;
import model.objects.Class;
import model.objects.Subject;

/**
 * @author Tomáš Hamsa on 14.09.2017.
 */
public final class SubjectDownloader {

  public static final String QUERY_NOT_CLOSED = "(enrollment==ALLOWED,parallelType==LECTURE)";

  public static final Predicate<Parallel> FILTER_NOTHING = parallel -> true;
  public static final Predicate<Parallel> FILTER_FULL = parallel ->
      parallel.getOccupied() < parallel.getCapacity()
          || parallel.getCapacityOverfill() == Permission.ALLOWED;

  private static final Function<Parallel, Stream<Class>> parallelToClass = parallel -> {
    List<TimetableSlot> slots = parallel.getTimetableSlot();

    if (slots == null) {
      return Stream.empty();
    }

    return slots.stream()
        .map(timetableSlot -> {

          Class newClass = new Class()
              .setType(convertType(parallel.getParallelType()));

          List<String> receivedTeachers = parallel.getTeacher();
          String teachers = receivedTeachers == null ? ""
              : receivedTeachers.stream().collect(Collectors.joining(", "));
          newClass.setTeacher(teachers);

          newClass.setDay(Days.getDay(timetableSlot.getDay() - 1));
          newClass.setStartHour((short) timetableSlot.getFirstHour());
          newClass.setEndHour((short) (newClass.getStartHour() + timetableSlot.getDuration() - 1));
          newClass.setRoom(timetableSlot.getRoom());

          return newClass;
        });
  };

  private SubjectDownloader() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  @Nonnull
  private static ClassType convertType(@Nonnull ParallelType parallelType) {
    switch (parallelType) {
      case LABORATORY:
        return ClassType.LABORATOR;
      case TUTORIAL:
        return ClassType.CVICENI;
      default:
        return ClassType.PREDNASKA;
    }
  }

  public static Subject download(@Nonnull ApiBase apiBase, @Nonnull String semester,
      @Nonnull String code, @Nonnull String name) throws IOException {

    Stream<Parallel> parallels = Kos.getCourseParallels(apiBase, semester, code, null);

    Subject subject = new Subject()
        .setCode(code)
        .setName(name);

    List<Class> classes = parallels.flatMap(parallelToClass).collect(Collectors.toList());

    return subject.setClasses(classes);
  }

  public static Subject download(@Nonnull ApiBase apiBase, @Nonnull String semester,
      @Nonnull String code) throws IOException {

    code = code.toUpperCase();

    String name = Kos.getCourseName(apiBase, semester, code);

    return download(apiBase, semester, code, name);
  }

  public static Subject download(@Nonnull ApiBase apiBase, @Nonnull String semester,
      @Nonnull String code, @Nonnull String name, @Nullable String query,
      @Nonnull Predicate<Parallel> filter) throws IOException {

    Stream<Parallel> parallels = Kos.getCourseParallels(apiBase, semester, code, query);

    Subject subject = new Subject()
        .setCode(code)
        .setName(name);

    List<Class> classes = parallels.filter(filter)
        .flatMap(parallelToClass).collect(Collectors.toList());

    return subject.setClasses(classes);
  }
}
