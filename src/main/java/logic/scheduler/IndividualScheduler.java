package logic.scheduler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import logic.schedule.IndividualSchedule;
import logic.schedule.PrioritizedClass;
import model.enums.ClassType;
import model.objects.Subject;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public final class IndividualScheduler extends Scheduler {

  private static final Logger LOG = Logger.getLogger(IndividualScheduler.class.getName());

  private final short theoryLessonPriority;
  private final short practicalLecturePriority;

  private final short earliestOKHour;
  private final short latestOKHour;
  private final double badHourPenalty;

  private final double crossingsMultiplier;
  private final double freeDaysMultiplier;
  private final double hoursMultiplier;

  private final boolean separateTheoryLessons;
  private int expandedStates;

  public IndividualScheduler(short theoryLessonPriority, short practicalLecturePriority,
      short earliestOKHour, short latestOKHour, double badHourPenalty, double crossingsMultiplier,
      double freeDaysMultiplier, double hoursMultiplier, boolean separateTheoryLessons) {

    this.theoryLessonPriority = theoryLessonPriority;
    this.practicalLecturePriority = practicalLecturePriority;
    this.earliestOKHour = earliestOKHour;
    this.latestOKHour = latestOKHour;
    this.badHourPenalty = badHourPenalty;
    this.crossingsMultiplier = Math.pow(10, crossingsMultiplier);
    this.freeDaysMultiplier = Math.pow(10, freeDaysMultiplier);
    this.hoursMultiplier = Math.pow(10, hoursMultiplier);
    this.separateTheoryLessons = separateTheoryLessons;
  }

  @Nonnull
  public Stream<IndividualSchedule> findBestSchedule(
      @Nonnull Collection<? extends Subject> subjects) {
    if (subjects.isEmpty()) {
      return Stream.empty();
    }

    List<Subject> subjectsWithClasses = filterEmptySubject(subjects);

    if (subjectsWithClasses.isEmpty()) {
      return Stream.empty();
    }

    IndividualSchedule schedule = new IndividualSchedule();

    subjectsWithClasses.stream()
        .flatMap(subject -> subject.getClasses().stream())
        .filter(clas -> clas.getType() == ClassType.PREDNASKA)
        .map(clas -> new PrioritizedClass(theoryLessonPriority, clas))
        .forEach(schedule::addClass);

    List<Subject> subjectWithLectures = filterTheoryLessons(subjectsWithClasses);

    if (subjectWithLectures.isEmpty()) {
      return Stream.of(schedule);
    }

    expandedStates = 0;
    timeStart = System.nanoTime();

    return addPracticalLectures(subjectWithLectures, schedule);
  }

  @Nonnull
  private Stream<IndividualSchedule> addPracticalLectures(@Nonnull List<Subject> subjects,
      @Nonnull IndividualSchedule initialSchedule) {

    Queue<ImmutablePair<IndividualSchedule, Integer>> queue = new PriorityQueue<>(
        1000, (a, b) -> Double.compare(evaluateSchedule(b.left), evaluateSchedule(a.left)));
    queue.add(new ImmutablePair<>(initialSchedule, 0));
    List<IndividualSchedule> results = new ArrayList<>(10);

    long endTIme = timeStart + TIME_LIMIT_NANOS;

    while (!queue.isEmpty() && endTIme > System.nanoTime()) {
      ++expandedStates;
      ImmutablePair<IndividualSchedule, Integer> positionPair = queue.remove();
      IndividualSchedule currentSchedule = positionPair.left;
      int position = positionPair.right;

      if (position == subjects.size()) {
        if (results.isEmpty() || !results.contains(currentSchedule)
            && evaluateSchedule(results.get(0)) <= evaluateSchedule(currentSchedule)) {
          results.add(currentSchedule);
          continue;
        }
        break;
      }

      int nextPosition = position + 1;

      subjects.get(position)
          .getClasses()
          .stream()
          .filter(clas -> clas.getType() != ClassType.PREDNASKA)
          .map(clas -> new PrioritizedClass(practicalLecturePriority, clas))
          .filter(clas -> currentSchedule.isSpaceEmpty(clas.getClazz().getDay().getValue(),
              clas.getClazz().getStartHour(), clas.getClazz().getEndHour(),
              clas.getPriority()))
          .map(clazz -> {
            IndividualSchedule newSchedule = currentSchedule.getCopy();
            newSchedule.addClass(clazz);
            return new ImmutablePair<>(newSchedule, nextPosition);
          })
          .filter(schedule -> !queue.contains(schedule))
          .forEach(queue::offer);
    }

    return results.stream();
  }

  private double evaluateSchedule(@Nonnull IndividualSchedule schedule) {

    double scheduleRating =
        (MAX_CROSSINGS - schedule.getCrossings()) * crossingsMultiplier +
            schedule.getFreeDaysCount() * freeDaysMultiplier

            + (MAXIMUM_SCHEDULE_HOURS - schedule
            .countHoursInSchool(earliestOKHour, latestOKHour, badHourPenalty)) * hoursMultiplier;

    if (separateTheoryLessons) {
      scheduleRating += schedule.getDaysWithOnlyTheoryLessonsCount() * 0.6 * freeDaysMultiplier;
    }

    return scheduleRating;
  }


  private void logCallsCount(@Nonnull List<Subject> subjectsWithLectures) {
    LOG.info(
        () -> String.format("%f seconds passed", (System.nanoTime() - timeStart) / 1000000000.0));
    long expectedCombinations = subjectsWithLectures.stream()
        .map(subject -> subject.getClasses().stream()
            .filter(aClass -> aClass.getType() != ClassType.PREDNASKA)
            .count())
        .reduce((a, b) -> a * b)
        .orElse(0L);
    LOG.info(() -> String.format("%d expected calls", expectedCombinations));
    long lecturesCount = subjectsWithLectures.stream()
        .flatMap(subject -> subject.getClasses().stream())
        .filter(aClass -> aClass.getType() != ClassType.PREDNASKA)
        .count();
    LOG.info(() -> String.format("%s lectures count", lecturesCount));
    double averageBranching = subjectsWithLectures.stream()
        .map(Subject::getClasses)
        .distinct()
        .mapToInt(Collection::size)
        .average()
        .orElse(0.0);
    LOG.info(() -> "Average branching factor " + averageBranching);
    LOG.info(() -> "Subjects count " + subjectsWithLectures.size());
    LOG.info(() -> "Expanded states " + expandedStates);
  }
}
