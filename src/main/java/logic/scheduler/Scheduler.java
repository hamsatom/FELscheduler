package logic.scheduler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import model.enums.ClassType;
import model.objects.Subject;

/**
 * @author Tomáš Hamsa on 08.10.2017.
 */
abstract class Scheduler {

  static final short MAXIMUM_SCHEDULE_HOURS = 85;
  static final int MAX_CROSSINGS = 10;
  static final long TIME_LIMIT_NANOS = 17L * 1000000000L;
  long timeStart;

  static List<Subject> filterEmptySubject(@Nonnull Collection<? extends Subject> subjects) {
    return subjects.stream()
        .filter(subject -> !subject.getClasses().isEmpty())
        .collect(Collectors.toList());
  }

  static List<Subject> filterTheoryLessons(@Nonnull Collection<? extends Subject> subjects) {
    return subjects.stream()
        .filter(subject -> subject.getClasses()
            .stream()
            .anyMatch(aClass -> aClass.getType() != ClassType.PREDNASKA))
        .collect(Collectors.toList());
  }
}
