package logic.scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Nonnull;
import logic.data.StudentService;
import logic.mapper.GroupScheduleConverter;
import logic.schedule.GroupSchedule;
import logic.schedule.PrioritizedClass;
import model.enums.ClassType;
import model.objects.Student;
import model.objects.Subject;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * @author Tomáš Hamsa on 01.10.2017.
 */
public final class GroupScheduler extends Scheduler {

  private static final Logger LOG = Logger.getLogger(GroupScheduler.class.getName());
  private static final ExecutorService THREAD = Executors.newSingleThreadExecutor();
  private static final double CLASSES_TOGETHER_MULTIPLIER = 1000000.0;
  private static final double LAYER_COEFFICIENT = 0.8;
  private static final double SCORE_COEFFICIENT = 0.99999;
  private long endTime;
  private volatile double[] layerScores;
  private int expandedStates;
  private int recursiveStates;

  private static List<ImmutablePair<Student, Subject>> convertData(
      @Nonnull Map<Student, List<Subject>> studentsAndSubjects) {
    return studentsAndSubjects.entrySet()
        .stream()
        .flatMap(pair -> pair.getValue().stream()
            .map(subject -> new ImmutablePair<>(pair.getKey(), subject)))
        .collect(Collectors.toList());
  }

  @Nonnull
  private static GroupSchedule createInitialSchedule(
      @Nonnull Map<Student, List<Subject>> studentsAndSubjects) {
    GroupSchedule groupSchedule = new GroupSchedule();
    studentsAndSubjects.forEach((s, p) -> p.stream()
        .flatMap(subject -> subject.getClasses().stream())
        .filter(aClass -> aClass.getType() == ClassType.PREDNASKA)
        .forEach(aClass -> groupSchedule
            .addClass(s, new PrioritizedClass(s.getTheoryLessonPriority(), aClass))));
    return groupSchedule;
  }

  private static Map<Student, List<Subject>> getData(@Nonnull Collection<? extends String> students,
      @Nonnull String semester) {
    return students.parallelStream()
        .map(StudentService::getStudentOrNew)
        .collect(Collectors.toConcurrentMap(s -> s,
            s -> StudentService.downloadEnrolledCourses(s, semester)));
  }

  private static void removeEmpty(@Nonnull Map<Student, List<Subject>> studentsAndSubjects) {
    studentsAndSubjects.replaceAll((student, subjects) -> filterEmptySubject(subjects));

    studentsAndSubjects.entrySet()
        .removeIf(pair -> pair.getValue()
            .isEmpty());
  }

  private static void removeTheoryLessons(
      @Nonnull Map<Student, List<Subject>> studentsAndSubjects) {
    studentsAndSubjects.values()
        .forEach(subjects -> subjects.forEach(subject ->
                subject.setClasses(
                    subject.getClasses()
                        .stream()
                        .filter(aClass -> aClass.getType() != ClassType.PREDNASKA)
                        .collect(Collectors.toList())
                )
            )
        );

    removeEmpty(studentsAndSubjects);
  }

  private static double evaluateSchedule(@Nonnull GroupSchedule schedule) {

    return schedule.getStudents()
        .mapToDouble(student -> {
          double scheduleRating =
              (MAX_CROSSINGS - schedule.getCrossings(student)) * student.getCrossingsMultiplicator()
                  +
                  schedule.getFreeDaysCount(student) * student.getFreeDaysMultiplicator()

                  + (MAXIMUM_SCHEDULE_HOURS - schedule
                  .countHoursInSchool(student, student.getEarliestOKHour(),
                      student.getLatestOKHour(),
                      student.getBadHourPenalty()))
                  * student.getHoursMultiplicator();

          if (student.isSeparateTheoryLessons()) {
            scheduleRating += schedule.getDaysWithOnlyTheoryLessonsCount(student) * 0.6 * student
                .getFreeDaysMultiplicator();
          }

          return scheduleRating;
        }).average()
        .orElse(0.0) + schedule.countClassesTogether() * CLASSES_TOGETHER_MULTIPLIER;
  }

  public List<Student> findBestSchedule(@Nonnull Collection<? extends String> students,
      @Nonnull String semester) {
    if (students.isEmpty()) {
      return Collections.emptyList();
    }

    Map<Student, List<Subject>> studentsAndSubjects = getData(students, semester);

    removeEmpty(studentsAndSubjects);

    if (studentsAndSubjects.isEmpty()) {
      return Collections.emptyList();
    }

    GroupSchedule groupSchedule = createInitialSchedule(studentsAndSubjects);

    removeTheoryLessons(studentsAndSubjects);

    List<ImmutablePair<Student, Subject>> pairs = convertData(studentsAndSubjects);

    Collection<GroupSchedule> builtSchedules = getResult(groupSchedule, pairs);

    return GroupScheduleConverter.convert(builtSchedules, semester);
  }

  private Collection<GroupSchedule> getResult(@Nonnull GroupSchedule groupSchedule,
      @Nonnull List<ImmutablePair<Student, Subject>> pairs) {

    double limits = evaluateSchedule(groupSchedule) * LAYER_COEFFICIENT;
    layerScores = new double[pairs.size()];
    Arrays.fill(layerScores, limits);

    expandedStates = 0;
    recursiveStates = 0;
    timeStart = System.nanoTime();
    endTime = timeStart + TIME_LIMIT_NANOS;

    Future<Collection<GroupSchedule>> branchBoundResult = THREAD
        .submit(() -> addLecturesBranchBound(pairs, groupSchedule, 0));

    Collection<GroupSchedule> builtSchedules = addPracticalLectures(pairs, groupSchedule);

    if (builtSchedules.isEmpty()) {
      try {
        builtSchedules = branchBoundResult.get();
      } catch (@Nonnull InterruptedException | ExecutionException e) {
        LOG.log(Level.INFO, "Error while getting result from branch and bound", e);
      }
    }
    branchBoundResult.cancel(true);

    return builtSchedules;
  }

  @Nonnull
  private Collection<GroupSchedule> addPracticalLectures(
      @Nonnull List<ImmutablePair<Student, Subject>> studentSubjects,
      @Nonnull GroupSchedule initialSchedule) {

    Queue<ImmutablePair<GroupSchedule, Integer>> queue = new PriorityQueue<>(4600,
        (a, b) -> Double.compare(evaluateSchedule(b.left), evaluateSchedule(a.left)));
    queue.add(new ImmutablePair<>(initialSchedule, 0));
    List<GroupSchedule> results = new ArrayList<>(4);

    while (endTime > System.nanoTime() && !queue.isEmpty()) {
      ++expandedStates;
      ImmutablePair<GroupSchedule, Integer> positionPair = queue.remove();
      GroupSchedule currentSchedule = positionPair.left;
      int position = positionPair.right;

      if (position == studentSubjects.size()) {
        if (results.isEmpty() || evaluateSchedule(results.get(0)) <= evaluateSchedule(
            currentSchedule)) {
          results.add(currentSchedule);
          continue;
        }
        return results;
      }

      int nextPosition = position + 1;
      ImmutablePair<Student, Subject> subjectPair = studentSubjects.get(position);
      Student student = subjectPair.left;

      subjectPair.right
          .getClasses()
          .stream()
          .map(clas -> new PrioritizedClass(student.getPracticalLecturePriority(), clas))
          .filter(
              clas -> currentSchedule.isSpaceEmpty(student, clas.getClazz().getDay().getValue(),
                  clas.getClazz().getStartHour(), clas.getClazz().getEndHour(),
                  clas.getPriority()))
          .map(clazz -> {
            GroupSchedule newSchedule = currentSchedule.getCopy();
            newSchedule.addClass(student, clazz);
            return new ImmutablePair<>(newSchedule, nextPosition);
          })
          .filter(groupSchedule -> !queue.contains(groupSchedule))
          .forEach(queue::offer);
    }

    return results;
  }

  @Nonnull
  private Set<GroupSchedule> addLecturesBranchBound(
      @Nonnull List<ImmutablePair<Student, Subject>> studentSubjects,
      @Nonnull GroupSchedule currentSchedule, int layer) {

    ++recursiveStates;
    if (studentSubjects.isEmpty() || endTime <= System.nanoTime()) {
      return Collections.singleton(currentSchedule);
    }

    int nextLayer = layer + 1;

    ImmutablePair<Student, Subject> pair = studentSubjects.get(0);
    Student student = pair.left;
    Subject curSubject = pair.right;

    Set<GroupSchedule> schedules = Collections.newSetFromMap(new ConcurrentHashMap<>(1));
    AtomicReference<Double> bestScore = new AtomicReference<>(Double.NEGATIVE_INFINITY);
    List<ImmutablePair<Student, Subject>> sublist = studentSubjects
        .subList(1, studentSubjects.size());

    curSubject.getClasses()
        .parallelStream()
        .map(clas -> new PrioritizedClass(student.getPracticalLecturePriority(), clas))
        .filter(
            clas -> currentSchedule.isSpaceEmpty(student, clas.getClazz().getDay().getValue(),
                clas.getClazz().getStartHour(), clas.getClazz().getEndHour(),
                clas.getPriority()))
        .map(clazz -> {
          GroupSchedule newSchedule = currentSchedule.getCopy();
          newSchedule.addClass(student, clazz);

          double scheduleScore = evaluateSchedule(newSchedule);
          if (layerScores[layer] < scheduleScore) {
            layerScores[layer] = scheduleScore;
          }

          return new ImmutablePair<>(newSchedule, scheduleScore);
        })
        .filter(scorePair -> scorePair.right > layerScores[layer] * SCORE_COEFFICIENT)
        .sorted((a, b) -> Double.compare(b.right, a.right))
        .map(newSchedule -> addLecturesBranchBound(sublist, newSchedule.left, nextLayer))
        .filter(groupSchedules -> !groupSchedules.isEmpty())
        .forEach(builtSchedules -> {
          double score = evaluateSchedule(builtSchedules.iterator().next());
          if (score > bestScore.get()) {
            bestScore.lazySet(score);
            schedules.clear();
            schedules.addAll(builtSchedules);
          } else if (score == bestScore.get()) {
            schedules.addAll(builtSchedules);
          }
        });

    return schedules;
  }

  private void logCallsCount(
      @Nonnull Map<Student, List<Subject>> subjectsWithLectures) {
    LOG.info(
        () -> String.format("%f seconds passed", (System.nanoTime() - timeStart) / 1000000000.0));
    LOG.info(() -> String.format("%d students with classes", subjectsWithLectures.size()));
    long expectedCombinations = subjectsWithLectures.values().stream()
        .map(subjects -> subjects.stream()
            .flatMap(subject -> subject.getClasses().stream())
            .filter(aClass -> aClass.getType() != ClassType.PREDNASKA)
            .count())
        .reduce((a, b) -> a * b)
        .orElse(0L);
    LOG.info(() -> String.format("%d expected calls", expectedCombinations));
    long lecturesCount = subjectsWithLectures.values().stream()
        .flatMap(Collection::stream)
        .flatMap(subject -> subject.getClasses().stream())
        .filter(aClass -> aClass.getType() != ClassType.PREDNASKA)
        .count();
    LOG.info(() -> String.format("%s lectures count", lecturesCount));
    double averageBranching = subjectsWithLectures.values()
        .stream()
        .flatMap(Collection::stream)
        .map(Subject::getClasses)
        .distinct()
        .mapToInt(Collection::size)
        .average()
        .orElse(0.0);
    LOG.info(() -> "Average branching factor " + averageBranching);
    int subjectsCount = subjectsWithLectures.values()
        .stream()
        .mapToInt(Collection::size)
        .sum();
    LOG.info(() -> "Subjects count " + subjectsCount);
    LOG.info(() -> "Expanded states " + expandedStates);
    LOG.info(() -> "Recursive states " + recursiveStates);
    LOG.info(() -> "Layer maxs" + System.lineSeparator() + IntStream.range(0, layerScores.length)
        .mapToObj(i -> i + " " + layerScores[i])
        .collect(Collectors.joining(System.lineSeparator())));
  }
}
