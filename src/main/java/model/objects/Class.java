package model.objects;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import model.enums.ClassType;
import model.enums.Days;
import model.enums.Locations;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public final class Class implements Serializable {

  private static final long serialVersionUID = -6786355111936390176L;

  private String teacher;
  private short startHour;
  private short endHour;
  private Days day;
  private Subject subject;
  @Nullable
  private String room;
  private ClassType type;

  public String getTeacher() {
    return teacher;
  }

  @Nonnull
  public Class setTeacher(String teacher) {
    this.teacher = teacher;
    return this;
  }

  public ClassType getType() {
    return type;
  }

  @Nonnull
  public Class setType(ClassType type) {
    this.type = type;
    return this;
  }

  @Nullable
  public String getRoom() {
    return room;
  }

  @Nonnull
  public Class setRoom(@Nullable String room) {
    this.room = room;
    return this;
  }

  public Subject getSubject() {
    return subject;
  }

  @Nonnull
  public Class setSubject(Subject subject) {
    this.subject = subject;
    return this;
  }


  public short getStartHour() {
    return startHour;
  }

  @Nonnull
  public Class setStartHour(short startHour) {
    this.startHour = startHour;
    return this;
  }

  public short getEndHour() {
    return endHour;
  }

  @Nonnull
  public Class setEndHour(short endHour) {
    this.endHour = endHour;
    return this;
  }

  @Nonnull
  public Locations getLocation() {
    if (room == null) {
      return Locations.UNDEFINED;
    } else if (room.startsWith("KN")) {
      return Locations.KARLOVO_NAMESTI;
    } else if (room.startsWith("T") || room.startsWith("Z")) {
      return Locations.DEJVICE;
    }
    return Locations.UNDEFINED;
  }

  public Days getDay() {
    return day;
  }

  @Nonnull
  public Class setDay(Days day) {
    this.day = day;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Class)) {
      return false;
    }
    Class aClass = (Class) o;
    return startHour == aClass.startHour && endHour == aClass.endHour && Objects
        .equals(teacher, aClass.teacher) && day == aClass.day && Objects.equals(room, aClass.room)
        && type == aClass.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(teacher, startHour, endHour, day, room, type);
  }
}