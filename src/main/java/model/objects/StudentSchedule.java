package model.objects;

import constant.ScheduleConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author Tomáš Hamsa on 01.10.2017.
 */
@Entity
public final class StudentSchedule implements Serializable {

  private static final long serialVersionUID = 1773183303090513546L;

  @Id
  @GeneratedValue
  private int id;

  @Column(nullable = false)
  @Basic(optional = false)
  private String studentsInGroup;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private short firstHour;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private short lastHour;

  @Column(nullable = false)
  @Basic(optional = false)
  private String semester;

  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<ScheduleEntry> classes = new ArrayList<>(0);

  public int getId() {
    return id;
  }

  public String getSemester() {
    return semester;
  }

  @Nonnull
  public StudentSchedule setSemester(String semester) {
    this.semester = semester;
    return this;
  }

  public String getStudentsInGroup() {
    return studentsInGroup;
  }

  @Nonnull
  public StudentSchedule setStudentsInGroup(String studentsInGroup) {
    this.studentsInGroup = studentsInGroup;
    return this;
  }

  @Nonnull
  public Class[][] getClasses() {
    Class[][] entries = new Class[ScheduleConstants.SCHOOL_DAYS_COUNT][ScheduleConstants.SCHOOL_HOURS_COUNT];
    classes.forEach(persistedEntry -> {
      Subject newSubject = new Subject()
          .setCode(persistedEntry.getSubjectCode())
          .setName(persistedEntry.getSubjectName());
      Class newClass = new Class()
          .setTeacher(persistedEntry.getTeacher())
          .setType(persistedEntry.getType())
          .setRoom(persistedEntry.getRoom())
          .setStartHour(persistedEntry.getStartHour())
          .setEndHour(persistedEntry.getEndHour())
          .setDay(persistedEntry.getDay())
          .setSubject(newSubject);
      for (int i = persistedEntry.getStartHour(); i <= persistedEntry.getEndHour(); i++) {
        entries[newClass.getDay().getValue()][i] = newClass;
      }
    });

    return entries;
  }

  @Nonnull
  public StudentSchedule setClasses(@Nonnull Class[][] classes) {
    this.classes = Arrays.stream(classes)
        .flatMap(Arrays::stream)
        .filter(Objects::nonNull)
        .map(aClass -> new ScheduleEntry()
            .setRoom(aClass.getRoom())
            .setSubjectCode(aClass.getSubject().getCode())
            .setSubjectName(aClass.getSubject().getName())
            .setTeacher(aClass.getTeacher())
            .setDay(aClass.getDay())
            .setStartHour(aClass.getStartHour())
            .setEndHour(aClass.getEndHour())
            .setType(aClass.getType()))
        .collect(Collectors.toList());
    return this;
  }

  public short getFirstHour() {
    return firstHour;
  }

  @Nonnull
  public StudentSchedule setFirstHour(short firstHour) {
    this.firstHour = firstHour;
    return this;
  }

  public short getLastHour() {
    return lastHour;
  }

  @Nonnull
  public StudentSchedule setLastHour(short lastHour) {
    this.lastHour = lastHour;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof StudentSchedule)) {
      return false;
    }
    StudentSchedule that = (StudentSchedule) o;
    return firstHour == that.firstHour &&
        lastHour == that.lastHour &&
        Objects.equals(studentsInGroup, that.studentsInGroup) &&
        Objects.equals(semester, that.semester);
  }

  @Override
  public int hashCode() {
    return Objects.hash(studentsInGroup, firstHour, lastHour, semester);
  }
}
