package model.objects;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import model.enums.ClassType;
import model.enums.Days;

/**
 * @author Tomáš Hamsa on 13.10.2017.
 */
@Entity
public final class ScheduleEntry implements Serializable {

  private static final long serialVersionUID = 3102122158019222987L;

  @Id
  @GeneratedValue
  private int id;

  @Basic
  private String subjectName;

  @Basic
  private String subjectCode;

  @Basic
  private String teacher;

  @Nullable
  @Basic
  private String room;

  private ClassType type;
  private Days day;

  @Basic
  private short startHour;

  @Basic
  private short endHour;

  public int getId() {
    return id;
  }

  public Days getDay() {
    return day;
  }

  @Nonnull
  public ScheduleEntry setDay(Days day) {
    this.day = day;
    return this;
  }

  public String getSubjectName() {
    return subjectName;
  }

  @Nonnull
  public ScheduleEntry setSubjectName(String subjectName) {
    this.subjectName = subjectName;
    return this;
  }

  public String getSubjectCode() {
    return subjectCode;
  }

  @Nonnull
  public ScheduleEntry setSubjectCode(String subjectCode) {
    this.subjectCode = subjectCode;
    return this;
  }

  public String getTeacher() {
    return teacher;
  }

  @Nonnull
  public ScheduleEntry setTeacher(String teacher) {
    this.teacher = teacher;
    return this;
  }

  @Nullable
  public String getRoom() {
    return room;
  }

  @Nonnull
  public ScheduleEntry setRoom(@Nullable String room) {
    this.room = room;
    return this;
  }

  public ClassType getType() {
    return type;
  }

  @Nonnull
  public ScheduleEntry setType(ClassType type) {
    this.type = type;
    return this;
  }

  public short getStartHour() {
    return startHour;
  }

  @Nonnull
  public ScheduleEntry setStartHour(short start) {
    startHour = start;
    return this;
  }

  public short getEndHour() {
    return endHour;
  }

  @Nonnull
  public ScheduleEntry setEndHour(short end) {
    endHour = end;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ScheduleEntry)) {
      return false;
    }
    ScheduleEntry that = (ScheduleEntry) o;
    return startHour == that.startHour && endHour == that.endHour && Objects
        .equals(subjectName, that.subjectName) && Objects.equals(subjectCode, that.subjectCode)
        && Objects.equals(teacher, that.teacher) && Objects.equals(room, that.room)
        && type == that.type && day == that.day;
  }

  @Override
  public int hashCode() {
    return Objects.hash(subjectName, subjectCode, teacher, room, type, day, startHour, endHour);
  }
}
