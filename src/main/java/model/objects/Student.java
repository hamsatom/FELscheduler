package model.objects;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * @author Tomáš Hamsa on 01.10.2017.
 */
@Entity
public final class Student implements Serializable {

  private static final long serialVersionUID = -5524346720423428169L;

  @Id
  private String code;

  @Nullable
  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @OrderBy("studentsInGroup ASC")
  private Set<StudentSchedule> schedules;

  @Column(nullable = false)
  @Basic(optional = false)
  private short earliestOKHour = 3;

  @Column(nullable = false)
  @Basic(optional = false)
  private short latestOKHour = 12;

  @Column(nullable = false)
  @Basic(optional = false)
  private double badHourPenalty = 1.5;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private double crossingsMultiplicator = 10;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private double freeDaysMultiplicator = 1000;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private double hoursMultiplicator = 100;

  @Column(nullable = false)
  @Basic(optional = false)
  private boolean separateTheoryLessons = true;

  @Column(nullable = false)
  @Basic(optional = false)
  private boolean useClosedClasses = true;

  @Column(nullable = false)
  @Basic(optional = false)
  private boolean useFullClasses = true;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private short theoryLessonPriority = 2;

  @Column(nullable = false)
  @Nonnegative
  @Basic(optional = false)
  private short practicalLecturePriority = 1;

  private static void isValidMultiplier(double value) {
    if (value != 10 && value != 100 && value != 1000) {
      throw new IllegalArgumentException("Multiplicator must be some power of 10");
    }
  }

  public boolean isUseClosedClasses() {
    return useClosedClasses;
  }

  @Nonnull
  public Student setUseClosedClasses(boolean useClosedClasses) {
    this.useClosedClasses = useClosedClasses;
    return this;
  }

  public boolean isUseFullClasses() {
    return useFullClasses;
  }

  @Nonnull
  public Student setUseFullClasses(boolean useFullClasses) {
    this.useFullClasses = useFullClasses;
    return this;
  }

  public short getTheoryLessonPriority() {
    return theoryLessonPriority;
  }

  @Nonnull
  public Student setTheoryLessonPriority(short theoryLessonPriority) {
    this.theoryLessonPriority = theoryLessonPriority;
    return this;
  }

  public short getPracticalLecturePriority() {
    return practicalLecturePriority;
  }

  @Nonnull
  public Student setPracticalLecturePriority(short practicalLecturePriority) {
    this.practicalLecturePriority = practicalLecturePriority;
    return this;
  }

  public String getCode() {
    return code;
  }

  @Nonnull
  public Student setCode(String code) {
    this.code = code;
    return this;
  }

  public short getEarliestOKHour() {
    return earliestOKHour;
  }

  @Nonnull
  public Student setEarliestOKHour(short earliestOKHour) {
    this.earliestOKHour = earliestOKHour;
    return this;
  }

  public short getLatestOKHour() {
    return latestOKHour;
  }

  @Nonnull
  public Student setLatestOKHour(short latestOKHour) {
    this.latestOKHour = latestOKHour;
    return this;
  }

  public double getBadHourPenalty() {
    return badHourPenalty;
  }

  @Nonnull
  public Student setBadHourPenalty(double badHourPenalty) {
    this.badHourPenalty = badHourPenalty;
    return this;
  }

  public double getCrossingsMultiplicator() {
    return crossingsMultiplicator;
  }

  @Nonnull
  public Student setCrossingsMultiplicator(double crossingsMultiplicator) {
    isValidMultiplier(crossingsMultiplicator);
    this.crossingsMultiplicator = crossingsMultiplicator;
    return this;
  }

  public double getFreeDaysMultiplicator() {
    return freeDaysMultiplicator;
  }

  @Nonnull
  public Student setFreeDaysMultiplicator(double freeDaysMultiplicator) {
    isValidMultiplier(freeDaysMultiplicator);
    this.freeDaysMultiplicator = freeDaysMultiplicator;
    return this;
  }

  public double getHoursMultiplicator() {
    return hoursMultiplicator;
  }

  @Nonnull
  public Student setHoursMultiplicator(double hoursMultiplicator) {
    isValidMultiplier(hoursMultiplicator);
    this.hoursMultiplicator = hoursMultiplicator;
    return this;
  }

  public boolean isSeparateTheoryLessons() {
    return separateTheoryLessons;
  }

  @Nonnull
  public Student setSeparateTheoryLessons(boolean separateTheoryLessons) {
    this.separateTheoryLessons = separateTheoryLessons;
    return this;
  }

  @Nonnull
  public Set<StudentSchedule> getSchedules() {
    if (schedules == null) {
      schedules = new HashSet<>();
    }
    return schedules;
  }

  @Nonnull
  public Student setSchedules(@Nullable Set<StudentSchedule> schedules) {
    this.schedules = schedules;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Student)) {
      return false;
    }
    Student student = (Student) o;
    return earliestOKHour == student.earliestOKHour &&
        latestOKHour == student.latestOKHour &&
        Double.compare(student.badHourPenalty, badHourPenalty) == 0 &&
        Double.compare(student.crossingsMultiplicator, crossingsMultiplicator) == 0 &&
        Double.compare(student.freeDaysMultiplicator, freeDaysMultiplicator) == 0 &&
        Double.compare(student.hoursMultiplicator, hoursMultiplicator) == 0 &&
        separateTheoryLessons == student.separateTheoryLessons &&
        useClosedClasses == student.useClosedClasses &&
        useFullClasses == student.useFullClasses &&
        theoryLessonPriority == student.theoryLessonPriority &&
        practicalLecturePriority == student.practicalLecturePriority &&
        Objects.equals(code, student.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, earliestOKHour, latestOKHour, badHourPenalty, crossingsMultiplicator,
        freeDaysMultiplicator, hoursMultiplicator, separateTheoryLessons, useClosedClasses,
        useFullClasses, theoryLessonPriority, practicalLecturePriority);
  }
}
