package model.objects;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public final class Subject implements Serializable {

  private static final long serialVersionUID = 102334622804516446L;
  private String name;
  private String code;
  @Nullable
  private List<Class> classes;

  @Nonnull
  public List<Class> getClasses() {
    if (classes == null) {
      return Collections.emptyList();
    }
    return classes;
  }

  @Nonnull
  public Subject setClasses(@Nullable List<Class> classes) {
    this.classes = classes;
    if (classes != null) {
      classes.forEach(clazz -> clazz.setSubject(this));
    }
    return this;
  }


  public String getName() {
    return name;
  }

  @Nonnull
  public Subject setName(String name) {
    this.name = name;
    return this;
  }

  public String getCode() {
    return code;
  }

  @Nonnull
  public Subject setCode(String code) {
    this.code = code;
    return this;
  }

  public String getNameShortcut() {
    if (code == null || code.length() < 2) {
      return name;
    }
    int shortcutStart;
    if (code.contains("-") && !code.endsWith("-")) {
      shortcutStart = code.indexOf('-') + 1;
    } else {
      shortcutStart = code.length() - 2;
      while (shortcutStart - 1 >= 0 && !Character.isDigit(code.charAt(shortcutStart - 1))) {
        --shortcutStart;
      }
    }
    return code.substring(shortcutStart);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Subject)) {
      return false;
    }
    Subject subject = (Subject) o;
    return Objects.equals(name, subject.name) && Objects.equals(code, subject.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, code);
  }

  @Override
  public String toString() {
    return getNameShortcut() + " - " + name;
  }

}
