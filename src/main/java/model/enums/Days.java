package model.enums;

import java.util.EnumMap;
import java.util.Map;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public enum Days {
  PONDELI(0),
  UTERY(1),
  STREDA(2),
  CTVRTEK(3),
  PATEK(4);

  private static final Days[] week = fillWeek();
  private static final Map<Days, String> nameMap = fillNames();
  private final int value;

  Days(int value) {
    this.value = value;
  }

  @Nonnull
  private static Days[] fillWeek() {
    Days[] week = new Days[5];
    week[0] = PONDELI;
    week[1] = UTERY;
    week[2] = STREDA;
    week[3] = CTVRTEK;
    week[4] = PATEK;
    return week;
  }

  @Nonnull
  private static Map<Days, String> fillNames() {
    Map<Days, String> names = new EnumMap<>(Days.class);
    names.put(PONDELI, "PONDĚLÍ");
    names.put(UTERY, "ÚTERÝ");
    names.put(STREDA, "STŘEDA");
    names.put(CTVRTEK, "ČTVRTEK");
    names.put(PATEK, "PÁTEK");
    return names;
  }

  public static Days getDay(int i) {
    return week[i];
  }

  public int getValue() {
    return value;
  }

  @Override
  public String toString() {
    return nameMap.getOrDefault(week[value], "");
  }
}
