package model.enums;

/**
 * @author Tomáš Hamsa on 14.09.2017.
 */
public enum ClassType {
  LABORATOR("laboratoř"), CVICENI("cvičení"), PREDNASKA("přednáška");

  private final String value;

  ClassType(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return value;
  }
}
