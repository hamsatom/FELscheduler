package model.enums;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public enum Locations {
  DEJVICE,
  KARLOVO_NAMESTI,
  UNDEFINED
}
