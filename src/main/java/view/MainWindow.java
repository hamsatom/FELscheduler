package view;

import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.annotation.Nonnull;
import view.util.InputValidation;

/**
 * @author Tomáš Hamsa on 16.07.2017.
 */
public final class MainWindow extends Application {
  private static final String MAIN_WINDOW_FORM = "/javaFxForms/MainWindow.fxml";

  public static void main(@Nonnull String[] args) {
    launch(args);
  }

  @Override
  public void start(@Nonnull Stage primaryStage) throws IOException {
    primaryStage.setOnCloseRequest(e -> {
      Platform.exit();
      System.exit(0);
    });

    Parent root = FXMLLoader.load(getClass().getResource(MAIN_WINDOW_FORM));
    primaryStage.setScene(new Scene(root));

    primaryStage.getIcons()
        .add(new Image(getClass().getClassLoader().getResourceAsStream(InputValidation.LOGO)));
    primaryStage.setTitle("Rozvrhář");

    primaryStage.show();

    primaryStage.setMaxHeight(primaryStage.getHeight());
    primaryStage.setMaxWidth(primaryStage.getWidth());
  }
}
