package view.controller;


import constant.ScheduleConstants;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import model.enums.ClassType;
import model.enums.Days;
import model.objects.Class;
import model.objects.Subject;
import org.apache.commons.lang3.StringUtils;
import view.element.CheckedHour;
import view.util.InputValidation;

/**
 * @author Tomáš Hamsa on 23.07.2017.
 */

public final class SubjectMaker implements Initializable {

  private static volatile Subject selectedSubject;

  @FXML
  public ComboBox<ClassType> classType;

  @FXML
  public TextField name;

  @FXML
  public TextField code;

  @FXML
  public TableView<List<CheckedHour>> currentClassTable;

  @FXML
  public TextField room;

  @FXML
  public TextField teacher;

  @FXML
  public TableView<Class> allClassesTable;

  @Nullable
  private Class editingClass;

  @Nullable
  static Subject getSelectedSubject() {
    return selectedSubject;
  }

  static void setSelectedSubject(@Nullable Subject selectedSubject) {
    SubjectMaker.selectedSubject = selectedSubject;
  }

  private static void closeWindow(@Nonnull Event event) {
    Node source = (Node) event.getSource();
    Stage stage = (Stage) source.getScene().getWindow();
    stage.close();
  }

  @Override
  public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    currentClassTable.getSelectionModel().setCellSelectionEnabled(true);

    fillClassTypes();
    createCurrentTable();
    createAllClassesTable();
    placeSelectedSubject();
  }

  private void fillClassTypes() {
    classType.setItems(FXCollections
        .observableArrayList(ClassType.values()));
    classType.getSelectionModel().select(ClassType.CVICENI);
  }

  private void placeSelectedSubject() {
    if (selectedSubject == null) {
      return;
    }
    name.setText(selectedSubject.getName());
    code.setText(selectedSubject.getCode());

    allClassesTable.setItems(FXCollections.observableArrayList(selectedSubject.getClasses()));
  }

  @SuppressWarnings("unchecked")
  private void createAllClassesTable() {

    TableColumn<Class, String> deleteColumn = new TableColumn<>();
    deleteColumn.setPrefWidth(Sizes.DELETE);
    deleteColumn.setMaxWidth(Sizes.DELETE);
    deleteColumn.setCellFactory(callback -> new TableCell<Class, String>() {
      final Button btn = new Button("Smazat");

      @Override
      public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
          setGraphic(null);
        } else {
          btn.setOnAction(event -> {
            allClassesTable.getItems().remove((Class) getTableRow().getItem());
            validateClasses();
          });
          setGraphic(btn);
        }
      }
    });

    TableColumn<Class, String> editColumn = new TableColumn<>();
    editColumn.setPrefWidth(Sizes.EDIT);
    editColumn.setMaxWidth(Sizes.EDIT);
    editColumn.setCellFactory(callback -> new TableCell<Class, String>() {
      final Button btn = new Button("Upravit");

      @Override
      public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
          setGraphic(null);
        } else {
          btn.setOnAction(event -> setEditingClass((Class) getTableRow().getItem()));
          setGraphic(btn);
        }
      }
    });

    allClassesTable.getColumns().addAll(editColumn, deleteColumn);
  }

  private void createCurrentTable() {
    TableColumn<List<CheckedHour>, String> firstColumn = new TableColumn<>();

    firstColumn.setCellValueFactory(data -> new ReadOnlyStringWrapper(
        data.getValue().stream()
            .filter(Objects::nonNull)
            .map(cls -> cls.getDay().toString())
            .findAny()
            .orElse("")));

    firstColumn.setSortable(false);
    firstColumn.setMaxWidth(Sizes.DAY);
    firstColumn.setPrefWidth(Sizes.DAY);
    currentClassTable.getColumns().add(firstColumn);

    for (int i = 1; i < ScheduleConstants.SCHOOL_HOURS_COUNT; i++) {

      TableColumn<List<CheckedHour>, Boolean> column = new TableColumn<>(Integer.toString(i));
      column.setSortable(false);

      int colIndex = i;
      column.setCellValueFactory(data -> {
        List<CheckedHour> rowValues = data.getValue();

        CheckedHour cellValue = null;
        if (colIndex < rowValues.size()) {
          cellValue = rowValues.get(colIndex);
        }

        return cellValue != null ? cellValue.checkedProperty() : new SimpleBooleanProperty(false);
      });

      column.setCellFactory(CheckBoxTableCell.forTableColumn(column));
      column.setEditable(true);
      column.setPrefWidth(Sizes.CHECKBOX_HOUR);
      column.setMaxWidth(Sizes.CHECKBOX_HOUR);

      currentClassTable.getColumns().add(column);
    }

    for (int i = 0; i < ScheduleConstants.SCHOOL_DAYS_COUNT; i++) {
      List<CheckedHour> dayHours = new ArrayList<>(ScheduleConstants.SCHOOL_HOURS_COUNT);

      for (int j = 0; j < ScheduleConstants.SCHOOL_HOURS_COUNT; j++) {
        dayHours.add(new CheckedHour((short) j, Days.getDay(i)));
      }

      currentClassTable.getItems().add(dayHours);
    }
  }

  private void setEditingClass(@Nonnull Class clazz) {
    editingClass = clazz;
    teacher.setText(clazz.getTeacher());
    room.setText(clazz.getRoom());
    classType.getSelectionModel().select(clazz.getType());

    List<List<CheckedHour>> hourBoxes = currentClassTable.getItems();
    // Uncheck all classes
    hourBoxes.forEach(day ->
        day.forEach(hour -> hour.setChecked(false)));
    List<CheckedHour> day = hourBoxes.get(clazz.getDay().getValue());
    for (int i = clazz.getStartHour(); i <= clazz.getEndHour(); i++) {
      day.get(i).setChecked(true);
    }
  }

  @FXML
  public void addClass() {
    if (!classFieldsValid()) {
      return;
    }

    List<CheckedHour> selectedHours = currentClassTable.getItems().stream()
        .flatMap(Collection::stream)
        .filter(hour -> hour.checkedProperty().get())
        .collect(Collectors.toList());

    CheckedHour firstSelected = selectedHours.get(0);

    short startHour = selectedHours.stream()
        .map(CheckedHour::getHour).min(
            Comparator.comparingInt(a -> a))
        .orElseGet(firstSelected::getHour);

    short endHour = selectedHours.stream()
        .map(CheckedHour::getHour)
        .max(Comparator.comparingInt(a -> a))
        .orElseGet(firstSelected::getHour);

    Class newClass = editingClass == null ? new Class() : editingClass;
    newClass.setRoom(room.getText())
        .setStartHour(startHour)
        .setEndHour(endHour)
        .setDay(firstSelected.getDay())
        .setType(classType.getSelectionModel().getSelectedItem());

    if (!StringUtils.isBlank(teacher.getText())) {
      newClass.setTeacher(teacher.getText());
    }

    if (editingClass == null) {
      allClassesTable.getItems().add(newClass);
    } else {
      allClassesTable.refresh();
    }
    validateClasses();
    resetClassCreation();
  }

  private boolean classFieldsValid() {

    List<CheckedHour> selectedHours = currentClassTable.getItems().stream()
        .flatMap(Collection::stream)
        .filter(hour -> hour.checkedProperty().get())
        .collect(Collectors.toList());

    boolean allAtSameDay = selectedHours.stream()
        .map(CheckedHour::getDay)
        .distinct().count() <= 1;

    List<Short> hours = selectedHours.stream()
        .map(CheckedHour::getHour)
        .sorted()
        .collect(Collectors.toList());

    boolean ascending = false;
    if (!hours.isEmpty()) {

      short previous = hours.get(0);
      ascending = true;

      for (int i = 1; i < hours.size(); i++) {

        short current = hours.get(i);
        if (current != previous + 1) {
          ascending = false;
          break;
        }

        previous = current;
      }
    }

    boolean classTimesOk = !selectedHours.isEmpty() && allAtSameDay && ascending;

    if (classTimesOk) {
      InputValidation.markValid(currentClassTable);
    } else {
      InputValidation.markInvalid(currentClassTable);
    }

    return classTimesOk & InputValidation.stringFieldsValid(room);
  }

  private void resetClassCreation() {

    currentClassTable.getItems().stream()
        .flatMap(Collection::stream)
        .forEach(hour -> hour.setChecked(false));
    teacher.setText("");
    room.setText("");
    editingClass = null;
  }

  public void close(@Nonnull Event event) {

    if (subjectFieldsValid() || classFieldsValid() || !allClassesTable.getItems().isEmpty()) {

      Alert alert = new Alert(AlertType.CONFIRMATION);
      alert.setContentText("Vyplněná data nebudou uložena. Chcete pokračovat?");

      ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons()
          .add(new Image(InputValidation.LOGO));
      alert.showAndWait();

      if (alert.getResult().getButtonData().isCancelButton()) {
        return;
      }
    }

    closeWindow(event);
  }

  public void saveSubject(@Nonnull Event event) {
    if (!subjectFieldsValid()) {
      return;
    }

    if (selectedSubject == null) {
      synchronized (SubjectMaker.class) {
        selectedSubject = new Subject();
      }
    }

    selectedSubject.setClasses(allClassesTable.getItems())
        .setCode(code.getText())
        .setName(name.getText());

    closeWindow(event);
  }

  private boolean subjectFieldsValid() {
    return validateClasses() & InputValidation.stringFieldsValid(name, code);
  }

  private boolean validateClasses() {

    boolean noClasses = allClassesTable.getItems().isEmpty();

    if (!noClasses) {
      InputValidation.markValid(allClassesTable);
    } else {
      InputValidation.markInvalid(allClassesTable);
    }

    return !noClasses;
  }

  public void textFieldValid(@Nonnull Event event) {
    TextField field = (TextField) event.getSource();
    InputValidation.stringFieldsValid(field);
  }

}
