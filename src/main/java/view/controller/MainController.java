package view.controller;

import static view.util.InputValidation.shrt;

import client.dto.objects.Parallel;
import client.request.ApiBase;
import client.request.Kos;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import logic.data.FileData;
import logic.data.StudentService;
import logic.mapper.SubjectDownloader;
import logic.multithreading.OrderingExecutor;
import logic.multithreading.RunnableTags;
import logic.schedule.IndividualSchedule;
import logic.scheduler.GroupScheduler;
import logic.scheduler.IndividualScheduler;
import model.enums.Days;
import model.objects.Class;
import model.objects.Student;
import model.objects.Subject;
import org.apache.commons.lang3.StringUtils;
import view.util.InputValidation;

/**
 * @author Tomáš Hamsa on 16.07.2017.
 */
public final class MainController implements Initializable {

  private static final Logger LOG = Logger.getLogger(MainController.class.getName());
  private static final String SUBJECT_MAKER_FORM = "/javaFxForms/SubjectMaker.fxml";
  private static final String KOS_WINDOW_FORM = "/javaFxForms/KosWindow.fxml";

  private static final String[] SCHEDULE_HOURS = {"", "7:30-8:15", "8:15-9:00", "9:15-10:00",
      "10:00-10:45", "11:00-11:45", "11:45-12:30", "12:45-13:30", "13:30-14:15", "14:30-15:15",
      "15:15-16:00", "16:15-17:00", "17:00-17:45", "18:00-18:45", "18:45-19:30", "19:45-20:30"};

  @FXML
  public Slider crossings;

  @FXML
  public Slider freeDays;

  @FXML
  public Slider hours;

  @FXML
  public TextField firstHour;

  @FXML
  public TextField lastHour;

  @FXML
  public TableView<Subject> subjects;

  @FXML
  public ListView<TableView<List<Class>>> schedules;

  @FXML
  public CheckBox separateTheory;

  @FXML
  public Slider penalty;

  @FXML
  public Slider practical;

  @FXML
  public Slider theory;

  @FXML
  public CheckBox includeFull;

  @FXML
  public CheckBox includeClosed;

  @FXML
  public CheckBox groupSeparateTheory;

  @FXML
  public Slider groupPenalty;

  @FXML
  public TextField groupLastHour;

  @FXML
  public TextField groupFirstHour;

  @FXML
  public Slider groupHours;

  @FXML
  public Slider groupFreeDays;

  @FXML
  public Slider groupCrossings;

  @FXML
  public Slider groupPractical;

  @FXML
  public Slider groupTheory;

  @FXML
  public ListView<TableView<List<Class>>> myGroupSchedules;

  @FXML
  public ListView<TableView<List<Class>>> newGroupSchedule;

  @FXML
  public TextField groupKosLogin;

  @FXML
  public TextField groupSemesterCode;

  @FXML
  public ListView<String> studentsInGroup;

  @FXML
  public CheckBox groupIncludeFull;

  @FXML
  public CheckBox groupIncludeClosed;

  @FXML
  public Hyperlink linkManual;

  @FXML
  public Hyperlink linkGit;

  private CheckBox selectAll;
  private List<Subject> selectedSubjects;
  private List<Student> updatedStudents;

  private static String getCellText(@Nonnull Class cellValue) {
    return Stream.of(cellValue.getSubject().getNameShortcut(), cellValue.getType().toString(),
        cellValue.getRoom(), cellValue.getTeacher())
        .filter(Objects::nonNull)
        .collect(Collectors.joining(System.lineSeparator()));
  }

  @SuppressWarnings("unchecked")
  private static void addMsgToTable(String msg,
      @Nonnull ListView<TableView<List<Class>>> schedules) {
    TableView<List<Class>> emptyTable = new TableView<>();
    TableColumn<List<Class>, String> column = new TableColumn<>();
    column.setCellValueFactory(clas -> new ReadOnlyStringWrapper(msg));

    emptyTable.getColumns().clear();
    emptyTable.getColumns().add(column);

    emptyTable.setItems(
        FXCollections.<List<Class>>observableArrayList(Collections.singletonList(new Class())));

    schedules.setItems(FXCollections.observableArrayList(emptyTable));
    schedules.refresh();
  }

  private static void addClasses(@Nonnull Class[][] classes,
      @Nonnull TableView<List<Class>> oneSchedule) {

    for (int i = 0; i < classes.length; i++) {

      if (Arrays.stream(classes[i])
          .allMatch(Objects::isNull)) {

        oneSchedule.getItems()
            .add(Collections.singletonList(new Class().setDay(Days.getDay(i))));

      } else {
        oneSchedule.getItems().add(Arrays.asList(classes[i]));
      }
    }
  }

  private static void setValueFactories(int start, int end,
      @Nonnull TableView<List<Class>> oneSchedule) {

    for (int i = start; i <= end; i++) {

      TableColumn<List<Class>, String> column = new TableColumn<>(
          String.format("%d. - %s", i, SCHEDULE_HOURS[i]));
      column.setSortable(false);

      int colIndex = i;

      column.setCellValueFactory(data -> {

        List<Class> rowValues = data.getValue();
        Class cellValue = null;

        if (colIndex < rowValues.size()) {
          cellValue = rowValues.get(colIndex);
        }

        String cellText = "";
        if (cellValue != null && cellValue.getSubject() != null) {
          cellText = getCellText(cellValue);
        }

        return new ReadOnlyObjectWrapper<>(cellText);
      });
      column.setMinWidth(Sizes.SCHEDULE_HOUR);

      oneSchedule.getColumns().add(column);
    }
  }

  private static void visitWebpage(String webpage) {
    try {
      Desktop.getDesktop().browse(new URI(webpage));
    } catch (IOException | URISyntaxException e) {
      LOG.log(Level.SEVERE, "Error while opening webpage " + webpage, e);
    }
  }

  @Override
  public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    OrderingExecutor.execute(() -> {
      try {
        addSubjectsToView(FileData.loadAllSubjects());
      } catch (Exception e) {
        LOG.log(Level.INFO, "Error while reading YAML in project folder", e);
      }
    }, null);

    if (!Desktop.isDesktopSupported()) {
      linkManual.setVisible(false);
      linkGit.setVisible(false);
    }
    selectedSubjects = new ArrayList<>();
    schedules.fixedCellSizeProperty().set(Sizes.SCHEDULE_HEIGHT + 10);
    groupSemesterCode.setText(Kos.getCurrentSemesterCode());
    studentsInGroup.setItems(FXCollections.observableArrayList(Collections.nCopies(13, "")));
    studentsInGroup.setCellFactory(TextFieldListCell.forListView());
    createSubjectsTable();
    fillEmpty();
    addMsgToTable("Zadejte KOS login pokud chcete zobrazit skupinové rozvrhy", myGroupSchedules);
  }

  @FXML
  public void visitGit() {
    visitWebpage("https://gitlab.fel.cvut.cz/hamsatom/FELscheduler");
  }

  @FXML
  public void visitManual() {
    visitWebpage("https://gitlab.fel.cvut.cz/hamsatom/FELscheduler/blob/master/Manual.pdf");
  }

  @SuppressWarnings("unchecked")
  private void createSubjectsTable() {
    subjects.setEditable(true);

    TableColumn<Subject, Boolean> checkBoxes = new TableColumn<>();
    checkBoxes.setPrefWidth(Sizes.CHECKBOX);
    checkBoxes.setMaxWidth(Sizes.CHECKBOX);
    checkBoxes.setCellValueFactory(e -> new SimpleBooleanProperty(selectedSubjects
        .contains(e.getValue())));

    selectAll = new CheckBox();
    checkBoxes.setGraphic(selectAll);
    selectAll.setPrefWidth(Sizes.CHECKBOX);
    selectAll.setMaxWidth(Sizes.CHECKBOX);
    selectAll.setOnAction(e -> {
      selectedSubjects.clear();
      if (selectAll.isSelected()) {
        selectedSubjects.addAll(subjects.getItems());
      }
      subjects.refresh();
      buildSchedules();
    });

    checkBoxes.setCellFactory(callback -> new TableCell<Subject, Boolean>() {
      final CheckBox checkBox = new CheckBox();

      @Override
      public void updateItem(@Nullable Boolean check, boolean empty) {
        super.updateItem(check, empty);

        if (check == null || empty) {
          setGraphic(null);

        } else {
          BooleanProperty checked = (BooleanProperty) callback.getCellObservableValue(getIndex());

          checkBox.setSelected(checked.get());
          checkBox.selectedProperty().bindBidirectional(checked);

          checkBox.setOnAction(e -> {
            if (checkBox.isSelected()) {
              selectedSubjects.add((Subject) getTableRow().getItem());
            } else {
              selectedSubjects.remove(getTableRow().getItem());
              selectAll.setSelected(false);
            }
            buildSchedules();
          });

          setGraphic(checkBox);
        }
      }
    });

    checkBoxes.setEditable(true);
    subjects.getColumns().add(0, checkBoxes);

    TableColumn<Subject, String> editColumn = new TableColumn<>();
    editColumn.setPrefWidth(Sizes.EDIT);
    editColumn.setMaxWidth(Sizes.EDIT);

    editColumn.setCellFactory(callback -> new TableCell<Subject, String>() {
      final Button btn = new Button("Upravit");

      @Override
      public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
          setGraphic(null);
        } else {
          btn.setOnAction(event -> {
            Subject editingSubject = (Subject) getTableRow().getItem();
            openMakeSubject(editingSubject);
          });
          setGraphic(btn);
        }
      }
    });

    TableColumn<Subject, String> deleteColumn = new TableColumn<>();
    deleteColumn.setPrefWidth(Sizes.DELETE);
    deleteColumn.setMaxWidth(Sizes.DELETE);

    deleteColumn.setCellFactory(callback -> new TableCell<Subject, String>() {
      final Button btn = new Button("Smazat");

      @Override
      public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
          setGraphic(null);
        } else {
          btn.setOnAction(event -> {
            Subject clickedSubject = (Subject) getTableRow().getItem();
            List<Subject> presentSubjects = subjects.getItems();
            presentSubjects.remove(clickedSubject);
            selectedSubjects.remove(clickedSubject);
            FileData.saveAllSubjects(presentSubjects.iterator());
          });
          setGraphic(btn);
        }
      }
    });

    subjects.getColumns().addAll(editColumn, deleteColumn);
  }

  @FXML
  public void buildSchedules() {
    if (InputValidation.shortFieldInvalid(firstHour, lastHour)) {
      return;
    }

    OrderingExecutor.execute(() -> {
      Platform.runLater(() -> addMsgToTable("Probíhá tvorba rozvrhu...", schedules));
      IndividualScheduler scheduler = new IndividualScheduler((short) theory.getValue(),
          (short) practical.getValue(),
          shrt(firstHour), shrt(lastHour),
          penalty.getValue() + 1.0,
          crossings.getValue(),
          freeDays.getValue(),
          hours.getValue(), separateTheory.isSelected());

      Stream<IndividualSchedule> builtSchedules = scheduler
          .findBestSchedule(selectedSubjects);
      Platform.runLater(() -> fillTable(builtSchedules));
    }, RunnableTags.BUILD_SCHEDULES);
  }

  private void fillTable(@Nonnull Stream<IndividualSchedule> builtSchedules) {

    fillEmpty();

    ObservableList<TableView<List<Class>>> filledTables = builtSchedules.map(schedule -> {
      TableView<List<Class>> oneSchedule = new TableView<>();
      oneSchedule.setMaxSize(Double.POSITIVE_INFINITY, Sizes.SCHEDULE_HEIGHT);
      oneSchedule.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

      TableColumn<List<Class>, String> firstColumn = new TableColumn<>();
      firstColumn.setCellValueFactory(data -> new ReadOnlyStringWrapper(
          data.getValue().stream()
              .filter(Objects::nonNull)
              .map(cls -> cls.getDay().toString())
              .findAny().orElse("")));

      firstColumn.setSortable(false);
      firstColumn.setMinWidth(Sizes.DAY);

      oneSchedule.getColumns().add(firstColumn);

      setValueFactories(schedule.getFirstUsedHour(), schedule.getLastUsedHour(), oneSchedule);

      addClasses(schedule.get(), oneSchedule);

      return oneSchedule;
    }).collect(Collectors.toCollection(FXCollections::observableArrayList));

    if (!filledTables.isEmpty()) {
      schedules.setItems(filledTables);
    }
  }

  private void fillEmpty() {

    boolean anyClasses = selectedSubjects.stream()
        .anyMatch(subject -> !subject.getClasses().isEmpty());

    String errorMsg;
    if (selectedSubjects.isEmpty()) {
      errorMsg = "Není vybrán žádný předmět. Vyberte nějaký, pokud chcete vytvořit rozvrh.";
    } else if (!anyClasses) {
      errorMsg = "Vybrané předměty namají žádnou hodinu. Přidejte nějakou nebo dovyberte předmět s hodinami";
    } else {
      errorMsg = "V rozvrhu je kolize a proto nejde sestavit. Změňte prioritu přednášek nebo odoznačte kolidující předměty";
    }

    addMsgToTable(errorMsg, schedules);
  }

  @FXML
  public void loadYamlFile() {

    FileChooser fileChooser = new FileChooser();
    fileChooser.setInitialDirectory(FileData.getSubjectsFile().getParent().toFile());
    fileChooser.setTitle("Najít YAML s předměty");

    File file = fileChooser.showOpenDialog(null);
    if (file == null) {
      return;
    }

    try {

      FileData.setSubjectsFile(file.getAbsolutePath());
      addSubjectsToView(FileData.loadAllSubjects());

    } catch (Exception e) {

      Alert alert = new Alert(AlertType.ERROR, "Nevalidní YAML soubor s předměty");

      ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons()
          .add(new Image(InputValidation.LOGO));
      alert.setHeaderText("Chyba v YAML souboru předmětů");

      alert.show();
    }
  }

  private void addSubjectsToView(@Nonnull Stream<Subject> loadedSubjects) {

    List<Subject> currentSubjects = subjects.getItems();

    List<Subject> toAdd = loadedSubjects.filter(subject -> !currentSubjects.contains(subject))
        .collect(Collectors.toList());

    toAdd.addAll(currentSubjects);

    toAdd.sort((a, b) -> a.getNameShortcut().compareToIgnoreCase(b.getNameShortcut()));

    subjects.setItems(FXCollections.observableArrayList(toAdd));

    if (!toAdd.isEmpty()) {
      selectAll.setSelected(false);
    }

    subjects.refresh();
  }

  @FXML
  public void updateParallels() {

    String query = includeClosed.isSelected() ? null : SubjectDownloader.QUERY_NOT_CLOSED;
    Predicate<Parallel> filter =
        includeFull.isSelected() ? SubjectDownloader.FILTER_NOTHING : SubjectDownloader.FILTER_FULL;

    OrderingExecutor.execute(() -> {

      List<Subject> toAdd = selectedSubjects.parallelStream()
          .map(subject -> {
            try {
              return SubjectDownloader
                  .download(ApiBase.FEL, Kos.getCurrentSemesterCode(), subject.getCode(),
                      subject.getName(), query, filter);

            } catch (@Nonnull Exception e) {
              LOG.log(Level.INFO, "Exception while updating subjects via KOS API", e);
            }
            return null;
          }).filter(Objects::nonNull)
          .collect(Collectors.toList());

      if (toAdd.size() < selectedSubjects.size()) {
        return;
      }

      List<Subject> displayedSubjects = subjects.getItems();

      displayedSubjects.removeAll(selectedSubjects);
      selectedSubjects.clear();
      selectedSubjects.addAll(toAdd);

      toAdd.addAll(displayedSubjects);
      toAdd.sort((a, b) -> a.getNameShortcut().compareToIgnoreCase(b.getNameShortcut()));
      subjects.setItems(FXCollections.observableArrayList(toAdd));

      subjects.refresh();
      FileData.saveAllSubjects(toAdd.iterator());
      buildSchedules();
    }, RunnableTags.UPDATE_PARALLELS);
  }

  @FXML
  public void openMakeSubject() throws IOException {

    Parent root = FXMLLoader.load(getClass().getResource(SUBJECT_MAKER_FORM));
    Stage stage = new Stage();
    stage.setScene(new Scene(root));

    stage.getIcons()
        .add(new Image(getClass().getClassLoader().getResourceAsStream(InputValidation.LOGO)));
    stage.setTitle("Tvorba předmětů");

    stage.setOnHidden(e -> {
      Subject selectedSubject = SubjectMaker.getSelectedSubject();

      if (selectedSubject == null) {
        return;
      }

      SubjectMaker.setSelectedSubject(null);
      selectedSubjects.remove(selectedSubject);
      addSubjectsToView(Stream.of(selectedSubject));
      FileData.saveAllSubjects(subjects.getItems().iterator());
    });

    stage.show();

    stage.setMaxWidth(stage.getWidth());
    stage.setMaxHeight(stage.getHeight());
  }

  private void openMakeSubject(@Nonnull Subject editingSubject) {
    try {
      SubjectMaker.setSelectedSubject(editingSubject);
      openMakeSubject();
    } catch (IOException e) {
      LOG.log(Level.SEVERE, "Error while opening SubjectMaker", e);
    }
  }

  @FXML
  public void showMyGroupSchedules() {
    if (!InputValidation.stringFieldsValid(groupKosLogin) | !InputValidation
        .semesterCodeValid(groupSemesterCode)) {
      return;
    }

    String login = groupKosLogin.getText();
    String semester = groupSemesterCode.getText();

    OrderingExecutor.execute(() -> {
      Platform.runLater(
          () -> addMsgToTable(
              "Hledají se rozvrhy uživatele " + login + " na semestr " + semester + " ...",
              myGroupSchedules));

      Optional<Student> foundStudent = StudentService.getDbStudent(login);
      if (!foundStudent.isPresent()) {
        Platform.runLater(() -> addMsgToTable("Uživatel " + login + " v semestru " + semester
                + " zatím nic neuložil, takže nemá žádné rozvrhy",
            myGroupSchedules));
        return;
      }
      Student realStudent = foundStudent.get();
      fillPreferences(realStudent);

      ObservableList<TableView<List<Class>>> filledTables = realStudent.getSchedules()
          .stream()
          .filter(studentSchedule -> semester.equals(studentSchedule.getSemester()))
          .map(schedule -> {
            TableView<List<Class>> oneSchedule = new TableView<>();
            oneSchedule.setMaxSize(Double.POSITIVE_INFINITY, Sizes.SCHEDULE_HEIGHT);
            oneSchedule.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            TableColumn<List<Class>, String> firstColumn = new TableColumn<>(
                schedule.getStudentsInGroup());
            firstColumn.setCellValueFactory(data -> new ReadOnlyStringWrapper(
                data.getValue().stream()
                    .filter(Objects::nonNull)
                    .map(cls -> cls.getDay().toString())
                    .findAny().orElse("")));

            firstColumn.setSortable(false);
            firstColumn.setMinWidth(Sizes.DAY);

            oneSchedule.getColumns().add(firstColumn);

            setValueFactories(schedule.getFirstHour(), schedule.getLastHour(), oneSchedule);

            addClasses(schedule.getClasses(), oneSchedule);

            return oneSchedule;
          }).collect(Collectors.toCollection(FXCollections::observableArrayList));

      if (!filledTables.isEmpty()) {
        Platform.runLater(() -> myGroupSchedules.setItems(filledTables));
      } else {
        Platform.runLater(
            () -> addMsgToTable(
                "Nenalezeny žádné rozvrhy, uživatel " + login + " v semestru " + semester
                    + " nemá rozvrh v žádné skupině, ale má uložené preference",
                myGroupSchedules));
      }

    }, RunnableTags.MY_GROUP_SCHEDULE);
  }

  private void fillPreferences(@Nonnull Student realStudent) {
    groupFirstHour.setText(String.valueOf(realStudent.getEarliestOKHour()));
    groupLastHour.setText(String.valueOf(realStudent.getLatestOKHour()));
    groupCrossings.setValue(Math.log10(realStudent.getCrossingsMultiplicator()));
    groupFreeDays.setValue(Math.log10(realStudent.getFreeDaysMultiplicator()));
    groupHours.setValue(Math.log10(realStudent.getHoursMultiplicator()));
    groupPenalty.setValue(realStudent.getBadHourPenalty() - 1.0);
    groupPractical.setValue(realStudent.getPracticalLecturePriority());
    groupTheory.setValue(realStudent.getTheoryLessonPriority());
    groupSeparateTheory.setSelected(realStudent.isSeparateTheoryLessons());
    groupIncludeClosed.setSelected(realStudent.isUseClosedClasses());
    groupIncludeFull.setSelected(realStudent.isUseFullClasses());
  }

  @FXML
  public boolean groupScheduleFieldsValid() {
    boolean groupNotEmpty = studentsInGroup.getItems()
        .stream()
        .anyMatch(s -> !StringUtils.isBlank(s));

    if (groupNotEmpty) {
      InputValidation.markValid(studentsInGroup);
    } else {
      InputValidation.markInvalid(studentsInGroup);
    }

    return groupNotEmpty & !InputValidation
        .shortFieldInvalid(groupFirstHour, groupLastHour)
        & InputValidation.semesterCodeValid(groupSemesterCode)
        & InputValidation.stringFieldsValid(groupKosLogin);
  }

  @FXML
  public void updatePreferences() {
    if (InputValidation.shortFieldInvalid(groupFirstHour, groupLastHour) | !InputValidation
        .stringFieldsValid(groupKosLogin)) {
      return;
    }

    Student newStudent = new Student().setCode(groupKosLogin.getText())
        .setBadHourPenalty(groupPenalty.getValue() + 1.0)
        .setTheoryLessonPriority((short) groupTheory.getValue())
        .setSeparateTheoryLessons(groupSeparateTheory.isSelected())
        .setPracticalLecturePriority((short) groupPractical.getValue())
        .setLatestOKHour(Short.parseShort(groupLastHour.getText()))
        .setEarliestOKHour(Short.parseShort(groupFirstHour.getText()))
        .setUseClosedClasses(groupIncludeClosed.isSelected())
        .setUseFullClasses(groupIncludeFull.isSelected())
        .setHoursMultiplicator(Math.pow(10, groupHours.getValue()))
        .setFreeDaysMultiplicator(Math.pow(10, groupFreeDays.getValue()))
        .setCrossingsMultiplicator(Math.pow(10, groupCrossings.getValue()));

    OrderingExecutor.execute(() -> StudentService.updatePreferences(newStudent), null);
  }

  @FXML
  public void buildGroupSchedules() {
    if (!groupScheduleFieldsValid()) {
      return;
    }

    Set<String> filteredGroup = studentsInGroup.getItems()
        .stream()
        .filter(s -> !StringUtils.isBlank(s))
        .collect(Collectors.toSet());
    filteredGroup.add(groupKosLogin.getText());

    if (filteredGroup.size() <= 1) {
      InputValidation.markInvalid(studentsInGroup);
      return;
    }

    String semester = groupSemesterCode.getText();

    OrderingExecutor.execute(() -> {
      Platform.runLater(
          () -> addMsgToTable(String.format("Staví se skupinový rozvrh pro %s na semester %s",
              filteredGroup.stream().collect(Collectors.joining(", ")), semester),
              newGroupSchedule));

      updatedStudents = new GroupScheduler().findBestSchedule(filteredGroup, semester);

      ObservableList<TableView<List<Class>>> filledTables = updatedStudents.stream()
          .flatMap(student -> student.getSchedules()
              .stream()
              .map(studentSchedule -> {
                    TableView<List<Class>> oneSchedule = new TableView<>();
                    oneSchedule.setMaxSize(Double.POSITIVE_INFINITY, Sizes.SCHEDULE_HEIGHT);
                    oneSchedule.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

                    TableColumn<List<Class>, String> firstColumn = new TableColumn<>(
                        student.getCode());
                    firstColumn.setCellValueFactory(data -> new ReadOnlyStringWrapper(
                        data.getValue().stream()
                            .filter(Objects::nonNull)
                            .map(cls -> cls.getDay().toString())
                            .findAny().orElse("")));

                    firstColumn.setSortable(false);
                    firstColumn.setMinWidth(Sizes.DAY);

                    oneSchedule.getColumns().add(firstColumn);

                    setValueFactories(studentSchedule.getFirstHour(), studentSchedule.getLastHour(),
                        oneSchedule);

                    addClasses(studentSchedule.getClasses(), oneSchedule);

                    return oneSchedule;
                  }
              )).collect(Collectors.toCollection(FXCollections::observableArrayList));

      if (!filledTables.isEmpty()) {
        Platform.runLater(() -> newGroupSchedule.setItems(filledTables));
      } else {
        Platform.runLater(
            () -> addMsgToTable(
                "Nešlo sestavit žádný rozvrh, někdo ze skupiny má v rozvrhu kolizi a nemá nastavenu prioritu cvičení na větší jak přednášek."
                    + System.lineSeparator() + "Nebo hodiny na semester " + semester
                    + " ještě nejsou vypsané.",
                newGroupSchedule));
      }

    }, RunnableTags.BUILD_GROUP_SCHEDULE);

  }

  @FXML
  public void assignGroupSchedules() {
    if (updatedStudents == null || updatedStudents.isEmpty()) {
      InputValidation.markInvalid(newGroupSchedule);
      return;
    }
    InputValidation.markValid(newGroupSchedule);
    OrderingExecutor.execute(() -> StudentService.addSchedulesToDBStudents(updatedStudents), null);
  }

  @FXML
  public void openKos() throws IOException {

    Parent root = FXMLLoader.load(getClass().getResource(KOS_WINDOW_FORM));
    Stage stage = new Stage();
    stage.setScene(new Scene(root));

    stage.getIcons()
        .add(new Image(getClass().getClassLoader().getResourceAsStream(InputValidation.LOGO)));
    stage.setTitle("KOS klient");

    stage.setOnHidden(e -> {
      Stream<Subject> downloadedSubjects = KosController.getDownloadedSubjects();

      if (downloadedSubjects == null) {
        return;
      }

      addSubjectsToView(
          downloadedSubjects.filter(Objects::nonNull)
      );
      FileData.saveAllSubjects(subjects.getItems().iterator());
    });

    stage.show();

    stage.setMaxHeight(stage.getHeight());
    stage.setMaxWidth(stage.getWidth());
  }
}
