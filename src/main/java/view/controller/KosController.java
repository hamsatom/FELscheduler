package view.controller;

import client.dto.enums.ClassesLang;
import client.dto.enums.Completion;
import client.dto.enums.ProgrammeType;
import client.dto.enums.Season;
import client.dto.enums.StudyForm;
import client.dto.objects.Course;
import client.dto.objects.InternalCourseEnrollment;
import client.dto.objects.Programme;
import client.request.ApiBase;
import client.request.Kos;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import logic.mapper.SubjectDownloader;
import logic.multithreading.OrderingExecutor;
import logic.multithreading.ResultHolder;
import logic.multithreading.RunnableTags;
import model.objects.Subject;
import org.apache.commons.lang3.StringUtils;
import view.util.InputValidation;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
public final class KosController implements Initializable {

  @Nullable
  private static Stream<Subject> downloadedSubjects;

  @FXML
  public TextField studentCode;

  @FXML
  public TextField semesterCode;

  @FXML
  public ComboBox<ApiBase> apiBase;

  @FXML
  public TableView<Programme> programmes;

  @FXML
  public ComboBox<ClassesLang> classesLang;

  @FXML
  public TextField code;

  @FXML
  public TextField diplomaName;

  @FXML
  public TextField faculty;

  @FXML
  public TextField studyDuration;

  @FXML
  public CheckBox openForAdmission;

  @FXML
  public TextField courseCode;

  @FXML
  public ComboBox<ProgrammeType> programmeType;

  @FXML
  public TableView<Course> programmeCourses;

  @FXML
  public ComboBox<ClassesLang> coursesClassesLang;

  @FXML
  public TextField coursesCode;

  @FXML
  public ComboBox<Completion> completion;

  @FXML
  public TextField credits;

  @FXML
  public TextField department;

  @FXML
  public TextField name;

  @FXML
  public ComboBox<ProgrammeType> type;

  @FXML
  public ComboBox<Season> season;

  @FXML
  public ComboBox<StudyForm> studyForm;

  @FXML
  public TableView<InternalCourseEnrollment> enrolledCourses;

  private List<Programme> selectedProgrammes;
  private List<InternalCourseEnrollment> selectedEnrollments;
  private List<Course> selectedProgrammeCourses;

  private Consumer<Stream<Course>> addProgrammeCourses;
  private Consumer<Stream<Course>> fillProgrammeCourses;

  @Nullable
  static Stream<Subject> getDownloadedSubjects() {
    return downloadedSubjects;
  }

  private static String buildQuery(@Nonnull Object... queryPairs) {
    if (queryPairs.length % 2 != 0) {
      throw new IllegalArgumentException("Not all pairs have key and value");
    }

    StringJoiner query = new StringJoiner(";");

    for (int i = 0; i < queryPairs.length - 1; i += 2) {

      Object key = queryPairs[i];
      Object value = queryPairs[i + 1];

      String valueString;
      if (value == null) {
        valueString = null;
      } else if (value instanceof String && !StringUtils.isBlank((String) value)) {
        valueString = "'*" + value + "*'";
      } else if (value instanceof Enum<?>) {
        valueString = ((Enum) value).name();
      } else {
        valueString = value.toString();
      }

      if (!StringUtils.isBlank(valueString)) {
        query.add(String.format("%s==%s", key, valueString));
      }
    }

    return query.toString();
  }

  private static void handleKosException(@Nonnull Exception e) {
    Platform.runLater(() -> {
      Alert alert = new Alert(AlertType.WARNING);
      alert.setHeaderText(String
          .format("Zadali jste něco špatně nebo KOS API vrátilo něco divného.%s",
              System.lineSeparator()));

      alert.setContentText(e.getLocalizedMessage());
      ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons()
          .add(new Image(InputValidation.LOGO));
      alert.show();
    });
  }

  private static void closeWindow(@Nonnull Event event) {
    Node source = (Node) event.getSource();
    Stage stage = (Stage) source.getScene().getWindow();
    stage.close();
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    selectedProgrammes = new ArrayList<>();
    selectedProgrammeCourses = new ArrayList<>();
    createProgrammeCoursesFunction();
    createProgrammesTable();

    initCourses();
    initProgrammes();

    String currentSemesterCode = Kos.getCurrentSemesterCode();
    semesterCode.setText(currentSemesterCode);
    fillComboBoxes();
    selectedEnrollments = new ArrayList<>();
    createEnrolledTable();
    createCoursesTable();

    synchronized (KosController.class) {
      downloadedSubjects = null;
    }
  }

  private void initProgrammes() {
    OrderingExecutor.execute(() -> {
      programmes.setDisable(true);
      fillProgrammes(ResultHolder.getProgrammes());
      programmes.setDisable(false);
    }, RunnableTags.PROGRAMMES);
  }

  private void initCourses() {
    OrderingExecutor.execute(() -> {
      programmeCourses.setDisable(true);
      fillProgrammeCourses.accept(ResultHolder.getCourses());
      programmeCourses.setDisable(false);
    }, RunnableTags.COURSES);
  }

  private void createProgrammeCoursesFunction() {
    addProgrammeCourses = courses -> {

      List<Course> currentCourses = programmeCourses.getItems();

      List<Course> newCourses = courses.filter(course -> !currentCourses.contains(course))
          .collect(Collectors.toList());

      newCourses.addAll(currentCourses);
      newCourses.sort((a, b) -> a.getName().compareToIgnoreCase(b.getName()));

      programmeCourses.setItems(FXCollections.observableArrayList(newCourses));
    };

    fillProgrammeCourses = courses -> programmeCourses.setItems(FXCollections.observableArrayList(
        courses.sorted((a, b) -> a.getName().compareToIgnoreCase(b.getName()))
            .collect(Collectors.toList())
    ));
  }

  private void createEnrolledTable() {
    enrolledCourses.setEditable(true);

    TableColumn<InternalCourseEnrollment, Boolean> checkBoxes = new TableColumn<>();

    CheckBox selectAll = new CheckBox();
    selectAll.setMaxWidth(Sizes.CHECKBOX);
    selectAll.setPrefWidth(Sizes.CHECKBOX);
    checkBoxes.setGraphic(selectAll);
    selectAll.setOnAction(e -> {
          if (selectAll.isSelected()) {
            selectedEnrollments.addAll(enrolledCourses.getItems());
          } else {
            selectedEnrollments.clear();
          }
          enrolledCourses.refresh();
        }
    );

    checkBoxes.setMaxWidth(Sizes.CHECKBOX);
    checkBoxes.setPrefWidth(Sizes.CHECKBOX);
    checkBoxes.setCellValueFactory(
        e -> new SimpleBooleanProperty(selectedEnrollments.contains(e.getValue())));

    checkBoxes.setCellFactory(callback -> new TableCell<InternalCourseEnrollment, Boolean>() {
      final CheckBox checkBox = new CheckBox();

      @Override
      public void updateItem(@Nullable Boolean check, boolean empty) {
        super.updateItem(check, empty);

        if (check == null || empty) {
          setGraphic(null);

        } else {
          BooleanProperty checked = (BooleanProperty) callback.getCellObservableValue(getIndex());

          checkBox.setSelected(checked.get());
          checkBox.selectedProperty().bindBidirectional(checked);

          checkBox.setOnAction(e -> {
            if (checkBox.isSelected()) {
              selectedEnrollments.add((InternalCourseEnrollment) getTableRow().getItem());
              InputValidation.markValid(enrolledCourses);
            } else {
              selectedEnrollments.remove((InternalCourseEnrollment) getTableRow().getItem());
              selectAll.setSelected(false);
            }
          });

          setGraphic(checkBox);
        }
      }
    });

    checkBoxes.setEditable(true);
    enrolledCourses.getColumns().add(0, checkBoxes);
  }

  private void createProgrammesTable() {
    programmes.setEditable(true);

    TableColumn<Programme, Boolean> checkBoxes = new TableColumn<>();
    checkBoxes.setMaxWidth(Sizes.CHECKBOX);
    checkBoxes.setPrefWidth(Sizes.CHECKBOX);
    checkBoxes.setCellValueFactory(
        e -> new SimpleBooleanProperty(selectedProgrammes.contains(e.getValue())));

    checkBoxes.setCellFactory(callback -> new TableCell<Programme, Boolean>() {
      final CheckBox checkBox = new CheckBox();

      @Override
      public void updateItem(@Nullable Boolean check, boolean empty) {
        super.updateItem(check, empty);

        if (check == null || empty) {
          setGraphic(null);

        } else {
          BooleanProperty checked = (BooleanProperty) callback.getCellObservableValue(getIndex());

          checkBox.setSelected(checked.get());
          checkBox.selectedProperty().bindBidirectional(checked);

          checkBox.setOnAction(e -> {
            if (checkBox.isSelected()) {
              Consumer<Stream<Course>> operation;
              if (selectedProgrammes.isEmpty()) {
                operation = fillProgrammeCourses;
              } else {
                operation = addProgrammeCourses;
              }
              Programme addedProgramme = (Programme) getTableRow().getItem();
              selectedProgrammes.add(addedProgramme);
              loadProgrammeCourses(operation, Collections.singletonList(addedProgramme));
            } else {
              selectedProgrammes.remove((Programme) getTableRow().getItem());
              loadProgrammeCourses(fillProgrammeCourses, selectedProgrammes);
            }
          });

          setGraphic(checkBox);
        }
      }
    });

    checkBoxes.setEditable(true);
    programmes.getColumns().add(0, checkBoxes);
  }

  private void createCoursesTable() {
    programmeCourses.setEditable(true);

    TableColumn<Course, Boolean> checkBoxes = new TableColumn<>();
    checkBoxes.setMaxWidth(Sizes.CHECKBOX);
    checkBoxes.setPrefWidth(Sizes.CHECKBOX);
    checkBoxes.setCellValueFactory(
        e -> new SimpleBooleanProperty(selectedProgrammeCourses.contains(e.getValue())));

    checkBoxes.setCellFactory(callback -> new TableCell<Course, Boolean>() {
      final CheckBox checkBox = new CheckBox();

      @Override
      public void updateItem(@Nullable Boolean check, boolean empty) {
        super.updateItem(check, empty);

        if (check == null || empty) {
          setGraphic(null);

        } else {
          BooleanProperty checked = (BooleanProperty) callback.getCellObservableValue(getIndex());

          checkBox.setSelected(checked.get());
          checkBox.selectedProperty().bindBidirectional(checked);

          checkBox.setOnAction(e -> {
            if (checkBox.isSelected()) {
              selectedProgrammeCourses.add((Course) getTableRow().getItem());
              InputValidation.markValid(programmeCourses);
            } else {
              selectedProgrammeCourses.remove((Course) getTableRow().getItem());
            }
          });

          setGraphic(checkBox);
        }
      }
    });

    checkBoxes.setEditable(true);
    programmeCourses.getColumns().add(0, checkBoxes);
  }

  private void fillComboBoxes() {
    apiBase.setItems(FXCollections.observableArrayList(ApiBase.values()));
    apiBase.getSelectionModel().select(ApiBase.FEL);

    Stream.of(classesLang, coursesClassesLang).forEach(lang -> lang.setItems(FXCollections
        .observableArrayList(ClassesLang.values())));

    Stream.of(programmeType, type).forEach(typeField -> typeField.setItems(FXCollections
        .observableArrayList(ProgrammeType.values())));

    completion.setItems(FXCollections.observableArrayList(Completion.values()));

    season.setItems(FXCollections.observableArrayList(Season.values()));

    studyForm.setItems(FXCollections.observableArrayList(StudyForm.values()));
  }

  private void fillProgrammes(@Nonnull Stream<Programme> loadedProgrammes) {

    programmes.setItems(FXCollections.observableArrayList(
        loadedProgrammes.filter(programme -> programme.getDiplomaName() != null)
            .sorted((a, b) -> a.getDiplomaName().compareToIgnoreCase(b.getDiplomaName()))
            .collect(Collectors.toList())
    ));

    selectedProgrammes.clear();
    programmes.refresh();
  }

  private void loadFilteredProgrammes() {

    boolean isFloat = true;
    float duration = 0.0f;

    try {
      duration = Float.parseFloat(studyDuration.getText());
    } catch (Exception e) {
      isFloat = false;
    }

    String filterQuery = buildQuery(classesLang.getId(),
        classesLang.getSelectionModel().getSelectedItem(),
        code.getId(), code.getText(),
        diplomaName.getId(), diplomaName.getText(),
        "faculty.name", faculty.getText(),
        openForAdmission.getId(), openForAdmission.isSelected(),
        studyDuration.getId(), isFloat ? duration : null,
        type.getId(), type.getSelectionModel().getSelectedItem());

    OrderingExecutor.execute(() -> {
      programmes.setDisable(true);
      try {
        Stream<Programme> loadedProgrammes = Kos
            .getAllProgrammes(apiBase.getSelectionModel().getSelectedItem(), semesterCode.getText(),
                filterQuery);
        fillProgrammes(loadedProgrammes);
      } catch (@Nonnull Exception e) {
        handleKosException(e);
      }
      programmes.setDisable(false);
    }, RunnableTags.PROGRAMMES);
  }

  private void loadProgrammeCourses(@Nonnull Consumer<Stream<Course>> action,
      @Nonnull Collection<Programme> programmes) {
    if (selectedProgrammes.isEmpty()) {
      loadAllCourses(apiBase.getSelectionModel().getSelectedItem());
      return;
    }

    boolean isShort = true;
    short creditsValue = 0;

    try {
      creditsValue = Short.parseShort(credits.getText());
    } catch (Exception e) {
      isShort = false;
    }

    String filterQuery = buildQuery("classesLang",
        coursesClassesLang.getSelectionModel().getSelectedItem(),
        "code", coursesCode.getText(),
        completion.getId(), completion.getSelectionModel().getSelectedItem(),
        credits.getId(), isShort ? creditsValue : null,
        "department.name", department.getText(),
        name.getId(), name.getText(),
        programmeType.getId(), programmeType.getSelectionModel().getSelectedItem(),
        season.getId(), season.getSelectionModel().getSelectedItem(),
        studyForm.getId(), studyForm.getSelectionModel().getSelectedItem());

    OrderingExecutor.execute(() -> {
      programmeCourses.setDisable(true);
      Stream<Course> courses = programmes.parallelStream()
          .flatMap(programme -> {
            try {
              return Kos.getProgrammeCourses(apiBase.getSelectionModel().getSelectedItem(),
                  semesterCode.getText(), filterQuery, programme.getCode());
            } catch (@Nonnull Exception e) {
              handleKosException(e);
            }
            return Stream.empty();
          }).distinct();

      action.accept(courses);
      selectedProgrammeCourses.clear();
      programmeCourses.refresh();
      programmeCourses.setDisable(false);
    }, RunnableTags.COURSES);
  }

  private void loadAllCourses(@Nonnull ApiBase apiLocation) {

    boolean isShort = true;
    short creditsValue = 0;

    try {
      creditsValue = Short.parseShort(credits.getText());
    } catch (Exception e) {
      isShort = false;
    }

    String filterQuery = buildQuery("classesLang",
        coursesClassesLang.getSelectionModel().getSelectedItem(),
        "code", coursesCode.getText(),
        completion.getId(), completion.getSelectionModel().getSelectedItem(),
        credits.getId(), isShort ? creditsValue : null,
        "department.name", department.getText(),
        name.getId(), name.getText(),
        programmeType.getId(), programmeType.getSelectionModel().getSelectedItem(),
        season.getId(), season.getSelectionModel().getSelectedItem(),
        studyForm.getId(), studyForm.getSelectionModel().getSelectedItem());

    OrderingExecutor.execute(() -> {
      programmeCourses.setDisable(true);
      try {
        ObservableList<Course> courses = FXCollections.observableArrayList(
            Kos.getAllCourses(apiLocation, semesterCode.getText(), filterQuery)
                .sorted((a, b) -> a.getName().compareToIgnoreCase(b.getName()))
                .collect(Collectors.toList())
        );

        Platform.runLater(() -> programmeCourses.setItems(courses));
      } catch (@Nonnull Exception e) {
        handleKosException(e);
      }

      selectedProgrammeCourses.clear();
      programmeCourses.refresh();
      programmeCourses.setDisable(false);
    }, RunnableTags.COURSES);
  }

  @FXML
  public void updateProgrammesFilter() {
    if (!isSemesterValid()) {
      return;
    }

    loadFilteredProgrammes();
  }

  @FXML
  public void updateCourseFilter() {
    if (!isSemesterValid()) {
      return;
    }

    loadProgrammeCourses(fillProgrammeCourses, selectedProgrammes);
  }

  @FXML
  public void getByLogin() {
    if (!canGetByLogin()) {
      return;
    }

    OrderingExecutor.execute(() -> {
      enrolledCourses.setDisable(true);
      try {
        enrolledCourses.setItems(FXCollections.observableArrayList(
            Kos.getStudentCourses(apiBase.getSelectionModel().getSelectedItem(),
                semesterCode.getText(), studentCode.getText())
                .sorted((a, b) -> a.getName().compareToIgnoreCase(b.getName()))
                .collect(Collectors.toList()))
        );
      } catch (@Nonnull Exception e) {
        handleKosException(e);
      }
      enrolledCourses.setDisable(false);
    }, RunnableTags.ENROLLMENTS);
  }

  public void saveEnrolledParallels(@Nonnull Event event) {
    if (!canGetByLogin() | !isEnrollmentSelected()) {
      return;
    }

    synchronized (KosController.class) {
      downloadedSubjects = selectedEnrollments.parallelStream()
          .map(enrollment -> {
            try {
              return SubjectDownloader
                  .download(apiBase.getSelectionModel().getSelectedItem(), semesterCode.getText(),
                      enrollment.getCourseCode(), enrollment.getName());
            } catch (@Nonnull Exception e) {
              handleKosException(e);
            }
            return null;
          }).filter(Objects::nonNull);
    }

    closeWindow(event);
  }

  public void saveCodeParallel(@Nonnull Event event) {
    if (!canGetByCourseCode()) {
      return;
    }

    try {
      synchronized (KosController.class) {
        downloadedSubjects = Stream.of(SubjectDownloader
            .download(apiBase.getSelectionModel().getSelectedItem(), semesterCode.getText(),
                courseCode.getText()));
      }

      closeWindow(event);

    } catch (@Nonnull Exception e) {
      handleKosException(e);
    }
  }

  public void saveProgrammeParallels(@Nonnull Event event) {
    if (!canGetByCourse()) {
      return;
    }

    synchronized (KosController.class) {
      downloadedSubjects = selectedProgrammeCourses.parallelStream()
          .map(course -> {
            try {
              return SubjectDownloader
                  .download(apiBase.getSelectionModel().getSelectedItem(), semesterCode.getText(),
                      course.getCode(), course.getName());
            } catch (@Nonnull Exception e) {
              handleKosException(e);
            }

            return null;

          }).filter(Objects::nonNull);
    }
    closeWindow(event);
  }

  private boolean isEnrollmentSelected() {
    boolean enrollmentSelected = !selectedEnrollments.isEmpty();

    if (enrollmentSelected) {
      InputValidation.markValid(enrolledCourses);
    } else {
      InputValidation.markInvalid(enrolledCourses);
    }

    return enrollmentSelected;
  }

  private boolean canGetByLogin() {
    return isSemesterValid() & InputValidation.stringFieldsValid(studentCode);
  }

  private boolean canGetByCourse() {
    boolean courseSelected = !selectedProgrammeCourses.isEmpty();

    if (!courseSelected) {
      InputValidation.markInvalid(programmeCourses);
    } else {
      InputValidation.markValid(programmeCourses);
    }

    return isSemesterValid() & courseSelected;
  }

  @FXML
  public boolean canGetByCourseCode() {
    return isSemesterValid() & InputValidation.stringFieldsValid(courseCode);
  }


  @FXML
  public boolean isSemesterValid() {
    return InputValidation.semesterCodeValid(semesterCode);
  }
}
