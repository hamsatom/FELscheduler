package view.controller;

/**
 * @author Tomáš Hamsa on 26.09.2017.
 */
final class Sizes {

  static final int SCHEDULE_HEIGHT = 371;
  static final int CHECKBOX = 24;
  static final int EDIT = 89;
  static final int DELETE = 89;
  static final int DAY = 73;
  static final int SCHEDULE_HOUR = 138;
  static final int CHECKBOX_HOUR = 35;

  private Sizes() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing constants holder");
  }
}
