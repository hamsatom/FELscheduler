package view;

import client.request.ApiBase;
import client.request.Kos;
import logic.data.StudentService;
import logic.multithreading.OrderingExecutor;
import logic.multithreading.ResultHolder;
import logic.multithreading.RunnableTags;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Launcher {
    private static final Logger LOG = Logger.getLogger(Launcher.class.getName());
    
    public static void main(String[] args) throws IOException {
        hackSystemEncoding();
        logToDB();
        signToApi();

        if (args.length > 0) {
            // Set logging to file for tests
            Logger.getLogger("").addHandler(new FileHandler("errors.log"));
        }

        MainWindow.main(args);
    }

    private static void logToDB() {
        OrderingExecutor.execute(() -> StudentService.getDbStudent("hamsatom"), null);
    }

    private static void hackSystemEncoding() {
        try {
            System.setProperty("file.encoding", "UTF-8");
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Exception while changing default system encoding", e);
        }
    }

    private static void signToApi() {
        String semester = Kos.getCurrentSemesterCode();
        OrderingExecutor.execute(() -> {
            try {
                ResultHolder.setCourses(Kos.getAllCourses(ApiBase.FEL, semester, null));
            } catch (Exception e) {
                LOG.log(Level.INFO, "Exception while downloading courses at start", e);
            }
        }, RunnableTags.COURSES);
        OrderingExecutor.execute(() -> {
            try {
                ResultHolder.setProgrammes(Kos.getAllProgrammes(ApiBase.FEL, semester, null));
            } catch (Exception e) {
                LOG.log(Level.INFO, "Exception while downloading programmes at start", e);
            }
        }, RunnableTags.PROGRAMMES);
    }
}
