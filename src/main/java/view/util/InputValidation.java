package view.util;

import java.util.Arrays;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Tomáš Hamsa on 30.07.2017.
 */
public final class InputValidation {

  public static final String LOGO = "graphic/ctu_logo.png";
  private static final String BACKGROUND_NORMAL = "-fx-text-box-border: grey";
  private static final String BACKGROUND_RED = "-fx-background-color: red";
  private static final int MINIMAL_LENGTH = 2;

  private InputValidation() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static short shrt(@Nonnull TextField field) {
    return Short.parseShort(field.getText());
  }

  public static boolean shortFieldInvalid(@Nonnull TextField... fields) {
    boolean allValid = true;
    for (TextField text : fields) {
      try {
        Short.parseShort(text.getText());
        markValid(text);
      } catch (@Nonnull NumberFormatException | NullPointerException e) {
        markInvalid(text);
        allValid = false;
      }
    }
    return !allValid;
  }

  public static boolean stringFieldsValid(@Nonnull TextField... fields) {
    boolean[] valid = {true};
    Arrays.stream(fields)
        .forEach(field -> {
          String fieldContent = field.getText();
          boolean invalid =
              StringUtils.isBlank(fieldContent) || fieldContent.length() < MINIMAL_LENGTH;
          if (invalid) {
            markInvalid(field);
            valid[0] = false;
          } else {
            markValid(field);
          }
        });
    return valid[0];
  }

  public static boolean semesterCodeValid(@Nonnull TextField semesterCode) {
    String semester = semesterCode.getText();

    boolean semesterValid = !StringUtils.isBlank(semester) &&
        semester.length() == 4 &&
        (semester.charAt(0) == 'A' ||
            semester.charAt(0) == 'B') &&
        Character.isDigit(semester.charAt(1)) &&
        Character.isDigit(semester.charAt(2)) &&
        (semester.charAt(3) == '1' ||
            semester.charAt(3) == '2');

    if (!semesterValid) {
      markInvalid(semesterCode);
    } else {
      markValid(semesterCode);
    }

    return semesterValid;
  }

  public static void markInvalid(@Nonnull Node node) {
    node.setStyle(BACKGROUND_RED);
  }

  public static void markValid(@Nonnull Node node) {
    node.setStyle(BACKGROUND_NORMAL);
  }
}
