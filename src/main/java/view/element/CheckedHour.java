package view.element;

import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javax.annotation.Nonnull;
import model.enums.Days;

/**
 * @author Tomáš Hamsa on 30.07.2017.
 */
public final class CheckedHour {

  private final BooleanProperty checked;
  private final short hour;
  private final Days day;

  public CheckedHour(short hour, @Nonnull Days day) {
    this.hour = hour;
    this.day = day;
    checked = new SimpleBooleanProperty(false);
  }

  @Nonnull
  public BooleanProperty checkedProperty() {
    return checked;
  }

  public short getHour() {
    return hour;
  }

  @Nonnull
  public Days getDay() {
    return day;
  }

  public void setChecked(boolean checked) {
    this.checked.set(checked);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CheckedHour)) {
      return false;
    }
    CheckedHour that = (CheckedHour) o;
    return hour == that.hour &&
        day == that.day;
  }

  @Override
  public int hashCode() {
    return Objects.hash(hour, day);
  }
}
