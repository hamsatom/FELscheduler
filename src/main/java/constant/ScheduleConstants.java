package constant;

/**
 * @author Tomáš Hamsa on 30.07.2017.
 */
public final class ScheduleConstants {

  public static final short SCHOOL_DAYS_COUNT = 5;
  public static final short SCHOOL_HOURS_COUNT = 17;

  private ScheduleConstants() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing constants holder");
  }
}
